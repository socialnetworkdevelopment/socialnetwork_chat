﻿namespace ApplicationServices.Interfaces.EventPublisher
{
    public interface IEventPublisher
    {
        Task PublishAsync<TNotification>(TNotification notification);
        Task PublishAsync<TNotification>(TNotification notification, PublishStrategy strategy);
        Task PublishAsync<TNotification>(TNotification notification, CancellationToken cancellationToken);
        Task PublishAsync<TNotification>(TNotification notification, PublishStrategy strategy, CancellationToken cancellationToken);
    }
}

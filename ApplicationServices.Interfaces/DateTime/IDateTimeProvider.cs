﻿namespace ApplicationServices.Interfaces
{
    public interface IDateTimeProvider
    {
        DateTime GetNow();
    }
}

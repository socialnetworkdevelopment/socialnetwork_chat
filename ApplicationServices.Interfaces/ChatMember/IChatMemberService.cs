﻿namespace ApplicationServices.Interfaces.ChatMember
{
    public interface IChatMemberService
    {
        Task<Entities.ChatMember> CreateAsync(Entities.Chat chat, Entities.User user);
    }
}

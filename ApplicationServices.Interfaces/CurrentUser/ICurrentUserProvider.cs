﻿namespace ApplicationServices.Interfaces.CurrentUser
{
    public interface ICurrentUserProvider<TUserId>
    {
        TUserId GetCurrentUserId();
    }
}

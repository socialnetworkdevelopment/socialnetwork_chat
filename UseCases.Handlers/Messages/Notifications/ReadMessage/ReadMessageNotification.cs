﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Handlers.Notifications.ReadMessage
{
    public class ReadMessageNotification : INotification
    {
        public ReadMessageNotification(ReadMessageDto readMessageDto)
        {
            this.Dto = Check.NotNull(readMessageDto);
        }

        public ReadMessageDto Dto { get; }
    }
}

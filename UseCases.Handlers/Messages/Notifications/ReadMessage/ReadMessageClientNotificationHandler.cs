﻿using ChatNotification.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace UseCases.Handlers.Notifications.ReadMessage
{
    public class ReadMessageClientNotificationHandler : INotificationHandler<ReadMessageNotification>
    {
        private readonly IServiceProvider _serviceProvider;

        public ReadMessageClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(ReadMessageNotification notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                await scope.ServiceProvider.GetRequiredService<IChatClientNotificator>()
                    .ReadMessageAsync(notification.Dto.MessageId, notification.Dto.ChatId);
            }
        }
    }
}

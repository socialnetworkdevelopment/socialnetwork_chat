﻿using MediatR;
using UseCases.Handlers.Dto;
using Utils;

namespace UseCases.Handlers.Notifications.SendMessage
{
    public class SendMessageNotification : INotification
    {
        public SendMessageNotification(SendMessageDto dto)
        {
            Dto = Check.NotNull(dto);
        }

        public SendMessageDto Dto { get; }
    }
}

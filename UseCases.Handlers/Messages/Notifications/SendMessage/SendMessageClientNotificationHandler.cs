﻿using ChatNotification.Interfaces;
using DataAccess.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace UseCases.Handlers.Notifications.SendMessage
{
    public class SendMessageClientNotificationHandler : INotificationHandler<SendMessageNotification>
    {
        private readonly IServiceProvider _serviceProvider;

        public SendMessageClientNotificationHandler(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(SendMessageNotification notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                var _userRepository = scope.ServiceProvider.GetRequiredService<IUserReadonlyRepository>();
                var author = await _userRepository.FirstOrDefaultAsync(u=>u.Id == notification.Dto.AuthorId);

                await scope.ServiceProvider.GetRequiredService<IChatClientNotificator>()
                    .SendMessageAsync(author, notification.Dto.ChatId, notification.Dto.Id, notification.Dto.Text, notification.Dto.SendAt);
            }
        }
    }
}

﻿using FluentValidation;
using UseCases.Handlers.Notifications.SendMessage;

namespace UseCases.Handlers.Validators
{
    public class SendMessageNotificationValidator : AbstractValidator<SendMessageNotification>
    {
        public SendMessageNotificationValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.AuthorId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.Text)
                .NotEmpty()
                .MaximumLength(4096);
        }
    }
}

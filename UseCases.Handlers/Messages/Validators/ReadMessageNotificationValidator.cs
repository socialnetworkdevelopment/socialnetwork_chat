﻿using FluentValidation;
using UseCases.Handlers.Notifications.ReadMessage;

namespace UseCases.Handlers.Validators
{
    public class ReadMessageNotificationValidator : AbstractValidator<ReadMessageNotification>
    {
        public ReadMessageNotificationValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.MessageId)
                .GreaterThan(0);
        }
    }
}

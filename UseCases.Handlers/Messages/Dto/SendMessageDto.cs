﻿namespace UseCases.Handlers.Dto
{
    public class SendMessageDto
    {
        public long Id { get; set; }

        public required string Text { get; set; }

        public int AuthorId { get; set; }

        public DateTime SendAt { get; set; }

        public int ChatId { get; set; }
    }
}

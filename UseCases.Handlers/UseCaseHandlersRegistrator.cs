﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Handlers.Notifications.ReadMessage;
using UseCases.Handlers.Notifications.SendMessage;
using UseCases.Validation;
using Utils;

namespace UseCases.Handlers
{
    public class UseCaseHandlersRegistrator : IServiceRegistrator
    {
        public void AddServices(IServiceCollection services)
        {
            services.AddSingleton<INotificationHandler<SendMessageNotification>, SendMessageClientNotificationHandler>()
                .Decorate(typeof(INotificationHandler<SendMessageNotification>), typeof(BaseValidatorNotificationHandler<SendMessageNotification>));

            services.AddSingleton<INotificationHandler<ReadMessageNotification>, ReadMessageClientNotificationHandler>()
                .Decorate(typeof(INotificationHandler<ReadMessageNotification>), typeof(BaseValidatorNotificationHandler<ReadMessageNotification>));
        }
    }
}

﻿using FluentValidation;
using MediatR;
using UtilsException = Utils.Exceptions;

namespace UseCases.Validation
{
    public class BaseValidatorNotificationHandler<TEvent> : INotificationHandler<TEvent>
        where TEvent : INotification
    {
        private readonly IEnumerable<IValidator<TEvent>> _validators;
        private readonly INotificationHandler<TEvent> _handler;

        public BaseValidatorNotificationHandler(IEnumerable<IValidator<TEvent>> validators, INotificationHandler<TEvent> handler)
        {
            _validators = validators;
            _handler = handler;
        }

        public async Task Handle(TEvent notification, CancellationToken cancellationToken)
        {
            if (!_validators.Any())
            {
                await _handler.Handle(notification, cancellationToken);
                return;
            }

            var context = new ValidationContext<TEvent>(notification);
            Dictionary<string, string[]> errorsDictionary = _validators
                .Select(x => x.Validate(context))
                .SelectMany(x => x.Errors)
                .Where(x => x != null)
                .GroupBy(
                    x => x.PropertyName,
                    x => x.ErrorMessage,
                    (propertyName, errorMessages) => new
                    {
                        Key = propertyName,
                        Values = errorMessages.Distinct().ToArray()
                    })
                .ToDictionary(x => x.Key, x => x.Values);

            if (errorsDictionary.Any())
            {
                throw new UtilsException.ValidationException(errorsDictionary);
            }

            await _handler.Handle(notification, cancellationToken);
        }
    }
}

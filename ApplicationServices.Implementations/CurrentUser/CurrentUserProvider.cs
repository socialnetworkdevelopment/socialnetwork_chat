﻿using ApplicationServices.Interfaces.CurrentUser;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.IdentityModel.Tokens.Jwt;
using Utils;

namespace ApplicationServices.Implementations.CurrentUser
{
    public class CurrentUserProvider : ICurrentUserProvider<long>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserProvider(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public long GetCurrentUserId()
        {
            if (_httpContextAccessor.HttpContext == null)
                throw new Exception("HttpContext doesn't exist.");
            
            string userIdStr = this.GetUserIdFromToken(_httpContextAccessor.HttpContext);
            if (!long.TryParse(userIdStr, out long userId))
                throw new Exception($"User ID {userIdStr} is not long.");
            return userId;
        }

        private string GetUserIdFromToken(HttpContext httpContext)
        {
            ArgumentNullException.ThrowIfNull(httpContext);

            var authorizationHeader = httpContext.Request.Headers["Authorization"].ToString();

            if (authorizationHeader != null && authorizationHeader.StartsWith("Bearer "))
            {
                var token = authorizationHeader["Bearer ".Length..].Trim();

                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token) as JwtSecurityToken;

                if (jsonToken != null)
                {
                    var userIdClaim = jsonToken.Claims.FirstOrDefault(claim => claim.Type == "sub");

                    if (userIdClaim != null)
                    {
                        return userIdClaim.Value;
                    }
                }
            }

            return string.Empty;
        }
    }

    namespace Registration
    {
        public class CurrentUserProviderRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
                => Check.NotNull(services).AddTransient<ICurrentUserProvider<long>, CurrentUserProvider>();
        }
    }
}

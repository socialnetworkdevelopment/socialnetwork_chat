﻿using ApplicationServices.Interfaces;
using ApplicationServices.Interfaces.ChatMember;
using DataAccess.Interfaces;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace ApplicationServices.Implementations.ChatMember
{
    public class ChatMemberService : IChatMemberService
    {
        private readonly IChatMemberRepository _chatMemberRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public ChatMemberService(IChatMemberRepository chatMemberRepository,
            IDateTimeProvider dateTimeProvider)
        {
            _chatMemberRepository = chatMemberRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<Entities.ChatMember> CreateAsync(Chat chat, User user)
        {
            Check.NotNull(chat);
            Check.NotNull(user);

            Entities.ChatMember? chatMember = 
                await _chatMemberRepository.FirstOrDefaultAsync(chatMember => chatMember.Chat.Id == chat.Id && chatMember.User.Id == user.Id);
            if (chatMember != null)
                throw new Exception($"User with id {user.Id} is already added to chat with id {chat.Id}");

            chatMember = new()
            {
                Chat = chat,
                User = user,
                AddedAt = _dateTimeProvider.GetNow()
            };
            await _chatMemberRepository.AddAsync(chatMember);
            await _chatMemberRepository.SaveAsync();

            return chatMember;
        }
    }

    namespace Registration
    {
        public class ChatMemberServiceRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IChatMemberService, ChatMemberService>();
            }
        }
    }
}

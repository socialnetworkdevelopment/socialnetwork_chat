﻿using AutoMapper;
using MassTransit;
using MediatR;
using SocialNetwork.Contracts.User;
using UseCases.Command.Update;
using UseCases.Command.UpdateAvatar;
using UseCases.Dto;

namespace MessageBus.MassTransit.Consumers.User
{
    public class UserAvatarUpdatedConsumer : IConsumer<UserAvatarUpdated>
    {
        private readonly IMediator _sender;

        public UserAvatarUpdatedConsumer(IMediator sender)
        {
            _sender = sender;
        }

        public async Task Consume(ConsumeContext<UserAvatarUpdated> context)
        {
            UserAvatarUpdated message = context.Message;
            UpdateUserAvatarCommand command = new (message.UserId, message.AvatarId);
            await _sender.Send(command);
        }
    }
}

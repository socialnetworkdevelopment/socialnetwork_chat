﻿using MassTransit;
using MediatR;
using SocialNetwork.Contracts.User;
using UseCases.Command.Create;

namespace MessageBus.MassTransit.Consumers
{
    public class UserDeletedConsumer : IConsumer<UserDeleted>
    {
        private readonly IMediator _sender;

        public UserDeletedConsumer(IMediator sender)
        {
            _sender = sender;
        }

        public async Task Consume(ConsumeContext<UserDeleted> context)
        {
            UserDeleted message = context.Message;
            await _sender.Send(new DeleteUserCommand(message.UserId));
        }
    }
}

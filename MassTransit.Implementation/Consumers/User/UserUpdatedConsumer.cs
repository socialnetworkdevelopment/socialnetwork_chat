﻿using AutoMapper;
using MassTransit;
using MediatR;
using SocialNetwork.Contracts.User;
using UseCases.Command.Update;
using UseCases.Dto;

namespace MessageBus.MassTransit.Consumers.User
{
    public class UserUpdatedConsumer : IConsumer<UserUpdated>
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public UserUpdatedConsumer(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<UserUpdated> context)
        {
            UserUpdated message = context.Message;
            UpdateUserCommand command = new(_mapper.Map<UpdateUserDto>(message));
            await _sender.Send(command);
        }
    }
}

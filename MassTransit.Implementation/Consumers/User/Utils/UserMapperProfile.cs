﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using SocialNetwork.Contracts.User;
using UseCases.Dto;
using Utils;

namespace MessageBus.MassTransit.Consumers.User.Utils
{
    internal class UserMapperProfile : Profile
    {
        public UserMapperProfile()
        {
            CreateMap<UserCreated, CreateUserDto>();    
            CreateMap<UserUpdated, UpdateUserDto>();
        }
    }

    namespace Registration
    {
        public class UserMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((serviceProvider, mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new UserMapperProfile());
                },
                Array.Empty<Type>());
            }
        }
    }
}

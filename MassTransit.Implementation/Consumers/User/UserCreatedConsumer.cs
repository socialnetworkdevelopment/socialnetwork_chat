﻿using AutoMapper;
using MassTransit;
using MediatR;
using SocialNetwork.Contracts.User;
using UseCases.Command.Create;
using UseCases.Dto;

namespace MessageBus.MassTransit.Consumers
{
    public class UserCreatedConsumer : IConsumer<UserCreated>
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public UserCreatedConsumer(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        public async Task Consume(ConsumeContext<UserCreated> context)
        {
            UserCreated message = context.Message;
            CreateUserCommand command = new(_mapper.Map<CreateUserDto>(message));
            await _sender.Send(command);
        }
    }
}

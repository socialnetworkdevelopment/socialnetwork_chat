﻿namespace Entities
{
    public class User : BaseEntity<long>
    {
        public required string FirstName { get; set; }

        public required string LastName { get; set; }

        public long? AvatarId { get; set; }
    }
}

﻿namespace Entities
{
    public class Message : BaseEntity<long>
    {
        public required User Author { get; set; }

        public required Chat Chat { get; set; }

        public required string Text { get; set; }

        public DateTime CreatedAt { get; set; }

        public bool IsRead { get; set; }
    }
}

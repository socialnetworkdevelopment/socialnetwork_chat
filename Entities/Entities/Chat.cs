﻿using Entities.Enums;

namespace Entities
{
    public class Chat : BaseEntity<int>
    {
        public string? Name { get; set; }

        public long? ImgId { get; set; }

        public DateTime CreatedAt { get; set; }
        
        public required User CreatedBy { get; set; }

        public required IList<ChatMember> Members { get; set; }

        public IList<Message>? Messages { get; set; }

        public ChatType Type{ get; set; }
    }
}

﻿namespace Entities
{
    public class GroupChatAdmin : BaseEntity<long>
    {
        public required Chat Chat { get; set; }

        public required User User { get; set; }

        public DateTime AddedAt { get; set; }
    }
}

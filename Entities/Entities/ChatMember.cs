﻿namespace Entities
{
    public class ChatMember : BaseEntity<int>
    {       
        public required Chat Chat { get; init; }
        
        public required User User { get; init; }

        public DateTime AddedAt { get; init; }
    }
}

﻿
using Entities;

namespace ChatNotification.Interfaces
{
    public interface IChatClientNotificator
    {
        Task SendMessageAsync(User author, int chatId, long messageId, string message, DateTime sendAt);

        Task ReadMessageAsync(long messageId, int chatId);

        Task JoinGroupAsync(int chatId, long userId);

        Task LeaveGroupAsync(int chatId, long userId);
    }
}
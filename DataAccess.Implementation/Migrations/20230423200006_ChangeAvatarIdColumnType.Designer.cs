﻿// <auto-generated />
using System;
using DataAccess.Implementation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace DataAccess.Implementation.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20230423200006_ChangeAvatarIdColumnType")]
    partial class ChangeAvatarIdColumnType
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("Entities.Chat", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("created_at");

                    b.Property<long>("CreatedById")
                        .HasColumnType("bigint")
                        .HasColumnName("created_by_id");

                    b.Property<long?>("ImgId")
                        .HasColumnType("bigint")
                        .HasColumnName("img_id");

                    b.Property<string>("Name")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("name");

                    b.Property<int>("Type")
                        .HasColumnType("integer")
                        .HasColumnName("type");

                    b.HasKey("Id")
                        .HasName("pk_chats");

                    b.HasIndex("CreatedById")
                        .HasDatabaseName("ix_chats_created_by_id");

                    b.ToTable("chats", (string)null);
                });

            modelBuilder.Entity("Entities.ChatMember", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<int>("Id"));

                    b.Property<DateTime>("AddedAt")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("added_at");

                    b.Property<int>("ChatId")
                        .HasColumnType("integer")
                        .HasColumnName("chat_id");

                    b.Property<long>("UserId")
                        .HasColumnType("bigint")
                        .HasColumnName("user_id");

                    b.HasKey("Id")
                        .HasName("pk_chat_members");

                    b.HasIndex("ChatId")
                        .HasDatabaseName("ix_chat_members_chat_id");

                    b.HasIndex("UserId")
                        .HasDatabaseName("ix_chat_members_user_id");

                    b.ToTable("chat_members", (string)null);
                });

            modelBuilder.Entity("Entities.GroupChatAdmin", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<DateTime>("AddedAt")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("added_at");

                    b.Property<int>("ChatId")
                        .HasColumnType("integer")
                        .HasColumnName("chat_id");

                    b.Property<long>("UserId")
                        .HasColumnType("bigint")
                        .HasColumnName("user_id");

                    b.HasKey("Id")
                        .HasName("pk_group_chat_admin");

                    b.HasIndex("ChatId")
                        .HasDatabaseName("ix_group_chat_admin_chat_id");

                    b.HasIndex("UserId")
                        .HasDatabaseName("ix_group_chat_admin_user_id");

                    b.ToTable("group_chat_admin", (string)null);
                });

            modelBuilder.Entity("Entities.Message", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long>("AuthorId")
                        .HasColumnType("bigint")
                        .HasColumnName("author_id");

                    b.Property<int>("ChatId")
                        .HasColumnType("integer")
                        .HasColumnName("chat_id");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("timestamp with time zone")
                        .HasColumnName("created_at");

                    b.Property<bool>("IsRead")
                        .HasColumnType("boolean")
                        .HasColumnName("is_read");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasMaxLength(4096)
                        .HasColumnType("character varying(4096)")
                        .HasColumnName("text");

                    b.HasKey("Id")
                        .HasName("pk_messages");

                    b.HasIndex("AuthorId")
                        .HasDatabaseName("ix_messages_author_id");

                    b.HasIndex("ChatId")
                        .HasDatabaseName("ix_messages_chat_id");

                    b.ToTable("messages", (string)null);
                });

            modelBuilder.Entity("Entities.User", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("id");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long?>("AvatarId")
                        .HasColumnType("bigint")
                        .HasColumnName("avatar_id");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("first_name");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasColumnType("text")
                        .HasColumnName("last_name");

                    b.HasKey("Id")
                        .HasName("pk_users");

                    b.ToTable("users", (string)null);

                    b.HasData(
                        new
                        {
                            Id = 1L,
                            FirstName = "John",
                            LastName = "Smith"
                        },
                        new
                        {
                            Id = 2L,
                            FirstName = "Miles",
                            LastName = "Morales"
                        },
                        new
                        {
                            Id = 3L,
                            FirstName = "Petr",
                            LastName = "Ivanov"
                        });
                });

            modelBuilder.Entity("Entities.Chat", b =>
                {
                    b.HasOne("Entities.User", "CreatedBy")
                        .WithMany()
                        .HasForeignKey("CreatedById")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_chats_users_created_by_id");

                    b.Navigation("CreatedBy");
                });

            modelBuilder.Entity("Entities.ChatMember", b =>
                {
                    b.HasOne("Entities.Chat", "Chat")
                        .WithMany("Members")
                        .HasForeignKey("ChatId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_chat_members_chats_chat_id");

                    b.HasOne("Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_chat_members_users_user_id");

                    b.Navigation("Chat");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Entities.GroupChatAdmin", b =>
                {
                    b.HasOne("Entities.Chat", "Chat")
                        .WithMany()
                        .HasForeignKey("ChatId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_group_chat_admin_chats_chat_id");

                    b.HasOne("Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_group_chat_admin_users_user_id");

                    b.Navigation("Chat");

                    b.Navigation("User");
                });

            modelBuilder.Entity("Entities.Message", b =>
                {
                    b.HasOne("Entities.User", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_messages_users_author_id");

                    b.HasOne("Entities.Chat", "Chat")
                        .WithMany("Messages")
                        .HasForeignKey("ChatId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired()
                        .HasConstraintName("fk_messages_chats_chat_id");

                    b.Navigation("Author");

                    b.Navigation("Chat");
                });

            modelBuilder.Entity("Entities.Chat", b =>
                {
                    b.Navigation("Members");

                    b.Navigation("Messages");
                });
#pragma warning restore 612, 618
        }
    }
}

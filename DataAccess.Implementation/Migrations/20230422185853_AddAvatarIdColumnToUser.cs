﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DataAccess.Implementation.Migrations
{
    /// <inheritdoc />
    public partial class AddAvatarIdColumnToUser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "img_link",
                table: "chats");

            migrationBuilder.AddColumn<long>(
                name: "avatar_id",
                table: "users",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "img_id",
                table: "chats",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 1L,
                column: "avatar_id",
                value: 0L);

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 2L,
                column: "avatar_id",
                value: 0L);

            migrationBuilder.UpdateData(
                table: "users",
                keyColumn: "id",
                keyValue: 3L,
                column: "avatar_id",
                value: 0L);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "avatar_id",
                table: "users");

            migrationBuilder.DropColumn(
                name: "img_id",
                table: "chats");

            migrationBuilder.AddColumn<string>(
                name: "img_link",
                table: "chats",
                type: "text",
                nullable: true);
        }
    }
}

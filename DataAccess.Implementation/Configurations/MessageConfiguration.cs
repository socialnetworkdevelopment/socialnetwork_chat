﻿using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation.Configurations
{
    internal class MessageConfiguration : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(message => message.Id);
            builder.Property(message => message.Text)
                .HasMaxLength(4096)
                .IsRequired();
            builder.HasOne(message => message.Chat)
                .WithMany(chat => chat.Messages)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

﻿using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation.Configurations
{
    internal class GroupChatAdminConfiguration : IEntityTypeConfiguration<GroupChatAdmin>
    {
        public void Configure(EntityTypeBuilder<GroupChatAdmin> builder)
        {
            builder.HasKey(groupChatAdmin => groupChatAdmin.Id);
            builder.HasOne(groupChatAdmin => groupChatAdmin.User)
                .WithMany()
                .HasForeignKey($"{nameof(User)}Id")
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(groupChatAdmin => groupChatAdmin.Chat)
                 .WithMany()
                 .HasForeignKey($"{nameof(Chat)}Id")
                 .IsRequired()
                 .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

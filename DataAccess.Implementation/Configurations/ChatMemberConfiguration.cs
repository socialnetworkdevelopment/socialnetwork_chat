﻿using Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Implementation.Configurations
{
    internal class ChatMemberConfiguration : IEntityTypeConfiguration<ChatMember>
    {
        public void Configure(EntityTypeBuilder<ChatMember> builder)
        {
            builder.HasKey(chatMember => chatMember.Id);
            builder.HasOne(chatMember => chatMember.Chat)
                .WithMany(chat => chat.Members)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

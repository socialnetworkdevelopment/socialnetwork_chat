﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using Entities;

namespace DataAccess.Implementation.Configurations
{
    internal class ChatConfiguration : IEntityTypeConfiguration<Chat>
    {
        public void Configure(EntityTypeBuilder<Chat> builder)
        {
            builder.Property(chat => chat.Name)
               .HasMaxLength(100);
            builder.HasKey(chat => chat.Id);
            builder.HasOne(chat => chat.CreatedBy)
                .WithMany()
                .IsRequired();
        }
    }
}

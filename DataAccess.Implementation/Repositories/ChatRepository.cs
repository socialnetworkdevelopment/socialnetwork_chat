﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation
{
    public class ChatRepository : Repository<Chat, int>, IChatRepository
    {
        public ChatRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class ChatRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IChatRepository, ChatRepository>();
                Check.NotNull(services).AddScoped<IChatReadonlyRepository, ChatRepository>();
            }
        }
    }
}

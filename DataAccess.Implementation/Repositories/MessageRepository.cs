﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation.Repositories
{
    public class MessageRepository : Repository<Message, long>, IMessageRepository
    {
        public MessageRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class MessageRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IMessageRepository, MessageRepository>();
                Check.NotNull(services).AddScoped<IMessageReadonlyRepository, MessageRepository>();
            }
        }
    }
}

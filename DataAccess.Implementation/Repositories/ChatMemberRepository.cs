﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation
{
    public class ChatMemberRepository : Repository<ChatMember, int>, IChatMemberRepository
    {
        public ChatMemberRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class ChatMemberRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IChatMemberRepository, ChatMemberRepository>();
                Check.NotNull(services).AddScoped<IChatMemberReadonlyRepository, ChatMemberRepository>();
            }
        }
    }
}

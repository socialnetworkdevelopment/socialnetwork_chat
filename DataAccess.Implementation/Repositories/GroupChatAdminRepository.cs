﻿using DataAccess.Interfaces;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace DataAccess.Implementation.Repositories
{
    public class GroupChatAdminRepository : Repository<GroupChatAdmin, long>, IGroupChatAdminRepository
    {
        public GroupChatAdminRepository(AppDbContext db) : base(db)
        {
        }
    }

    namespace Registration
    {
        public class GroupChatAdminRepositoryRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddScoped<IGroupChatAdminRepository, GroupChatAdminRepository>();
                Check.NotNull(services).AddScoped<IGroupChatAdminReadonlyRepository, GroupChatAdminRepository>();
            }
        }
    }
}

﻿using Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DataAccess.Implementation
{
    public class AppDbContext : DbContext
    {
        private readonly ILogger<AppDbContext> _logger;

        public AppDbContext(DbContextOptions<AppDbContext> options, ILogger<AppDbContext> logger) : base(options)
        {
            _logger = logger;
        }

        public DbSet<Chat> Chats { get; set; } = null!;

        public DbSet<ChatMember> ChatMembers { get; set; } = null!;

        public DbSet<User> Users { get; set; } = null!;

        public DbSet<Message> Messages { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            this.ApplyNewMigrations();

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(AppDbContext).Assembly);
        }

        private void ApplyNewMigrations()
        {
            try
            {
                if (this.Database.GetPendingMigrations().Count() != 0)
                {
                    this.Database.Migrate();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception while migration.");
            }
        }
    }
}

﻿using Entities;
using System.Linq.Expressions;

namespace DataAccess.Interfaces
{
    public interface IReadonlyRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        Task<TEntity?> FindAsync(TKey id);

        Task<IEnumerable<TEntity>> GetAllAsync(
            Expression<Func<TEntity, bool>>? filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null,
            List<string>? includeProperties = null,
            int? take = null,
            int? skip = null,
            bool descending = false,
            bool isTracking = true
            );

        Task<TEntity?> FirstOrDefaultAsync(
            Expression<Func<TEntity, bool>>? filter = null,
            List<string>? includeProperties = null,
            bool isTracking = true
            );

        Task<int> CountAsync(Expression<Func<TEntity, bool>>? filter = null);
    }
}

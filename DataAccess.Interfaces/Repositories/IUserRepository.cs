﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IUserRepository : IRepository<User, long>, IUserReadonlyRepository
    {
    }
}

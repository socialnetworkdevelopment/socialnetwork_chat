﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IGroupChatAdminReadonlyRepository : IReadonlyRepository<GroupChatAdmin, long>
    {
    }
}

﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IMessageReadonlyRepository : IReadonlyRepository<Message, long>
    {
    }
}

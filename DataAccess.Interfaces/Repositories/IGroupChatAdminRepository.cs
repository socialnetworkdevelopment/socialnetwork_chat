﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IGroupChatAdminRepository : IRepository<GroupChatAdmin, long>, IGroupChatAdminReadonlyRepository
    {
    }
}

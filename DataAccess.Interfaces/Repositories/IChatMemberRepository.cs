﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IChatMemberRepository : IRepository<ChatMember, int>, IChatMemberReadonlyRepository
    {
    }
}

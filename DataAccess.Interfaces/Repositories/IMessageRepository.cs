﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IMessageRepository : IRepository<Message, long>, IMessageReadonlyRepository
    {
    }
}

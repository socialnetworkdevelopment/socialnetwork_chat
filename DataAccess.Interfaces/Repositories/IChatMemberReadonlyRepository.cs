﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IChatMemberReadonlyRepository : IReadonlyRepository<ChatMember, int>
    {
    }
}

﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IChatReadonlyRepository : IReadonlyRepository<Chat, int>
    {
    }
}

﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IChatRepository : IRepository<Chat, int>, IChatReadonlyRepository
    {
    }
}

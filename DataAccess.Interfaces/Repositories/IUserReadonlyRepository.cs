﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IUserReadonlyRepository : IReadonlyRepository<User, long>
    {
    }
}

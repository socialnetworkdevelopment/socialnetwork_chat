﻿using Entities;

namespace DataAccess.Interfaces
{
    public interface IRepository<TEntity, TKey> : IReadonlyRepository<TEntity, TKey>
        where TEntity : BaseEntity<TKey>
    {
        Task<TEntity> AddAsync(TEntity entity);

        void Remove(TEntity entity);

        void RemoveRange(IEnumerable<TEntity> entities);

        TEntity Update(TEntity entity);

        Task SaveAsync();
    }
}

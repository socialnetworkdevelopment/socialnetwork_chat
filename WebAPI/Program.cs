using DataAccess.Implementation;
using Hub.SignalR.Implementation;
using Microsoft.EntityFrameworkCore;
using Npgsql;
using SocialNetwork.Common.CorrelationId;
using SocialNetwork.Common.IdentityServer;
using System.Reflection;
using Utils;
using Utils.DependencyInjection;
using WebAPI;

List<Assembly> dummyAssemblyList = new()
            {
                typeof(ApplicationServices.Implementations.AssemblyMarker).Assembly,
                typeof(UseCases.AssemblyMarker).Assembly,
                typeof(MessageBus.MassTransit.AssemblyMarker).Assembly,
                typeof(Controllers.AssemblyMarker).Assembly,
                typeof(Hub.SignalR.AssemblyMarker).Assembly,
                typeof(UseCases.Handlers.AssemblyMarker).Assembly,
            };

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//Infrastructure
builder.Services.AddDbContext<AppDbContext>(options =>
                            options.UseNpgsql(new NpgsqlConnection(builder.Configuration.GetConnectionString(Consts.ChatConnectionString)))
                            .UseSnakeCaseNamingConvention());

//UseCases & Application
builder.Services.AddByRegistrators();
builder.Services.AddValidators();

//Frameworks
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.CustomSchemaIds(type => type.ToString());
    options.AddSignalRSwaggerGen(action => action.ScanAssembly(typeof(Hub.SignalR.AssemblyMarker).Assembly));
});

builder.Services.AddCorrelationIdAdditionalServices();

string? identityServerUrl = builder.Configuration.GetValue<string>(Consts.IdentityServerUrlConfig);
if (string.IsNullOrEmpty(identityServerUrl))
    throw new Exception($"Not found value for config key {Consts.IdentityServerUrlConfig}");
builder.Services.AddIdentityServer(identityServerUrl);

builder.Services.AddHttpContextAccessor();

// Specifying the configuration for serilog
SocialNetwork.Common.Logging.LogConfigurator.ConfigureSerilogToSeq(builder.Host, builder.Configuration);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();

string? reactClientrUrl = builder.Configuration.GetValue<string>(Consts.ReactClientrUrlConfig);
if (string.IsNullOrEmpty(reactClientrUrl))
    throw new Exception($"Not found value for config key {Consts.ReactClientrUrlConfig}");
app.UseCorsForClients(reactClientrUrl);

app.UseMiddleware<WebSocketsMiddleware>();
app.UseAuthentication();
app.UseAuthorization();


app.MapControllers();
app.MapHub<ChatHub>("/chathub");

app.UseExceptionMiddleware();
app.UseCorrelationIdMiddleware();

app.UseHttpsRedirection();


app.Run();

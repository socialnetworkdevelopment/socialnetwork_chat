﻿using Newtonsoft.Json;
using System.Net;
using Utils;
using Utils.Exceptions;

namespace WebAPI
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ValueMissingException ex)
            {
                httpContext.Response.ContentType = "application/json";

                string result = new ErrorDetails()
                {
                    Message = ex.Message,
                    StatusCode = (int)HttpStatusCode.NotFound
                }.ToString();

                httpContext.Response.StatusCode = (int)HttpStatusCode.NotFound;
                await httpContext.Response.WriteAsync(result);
            }
            catch (ValidationException ex)
            {
                await HandleValidateExceptionAsync(httpContext, ex);
            }
        }

        private static async Task HandleValidateExceptionAsync(HttpContext httpContext, Exception exception)
        {
            var response = new
            {
                StatusCode = StatusCodes.Status400BadRequest,
                Message = exception.Message,
                ValidationErrors = GetErrors(exception)
            };
            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;
            await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(response));
        }

        private static IReadOnlyDictionary<string, string[]> GetErrors(Exception exception)
        {
            IReadOnlyDictionary<string, string[]> errors = null;
            if (exception is ValidationException validationException)
            {
                errors = validationException.ErrorsDictionary;
            }
            return errors;
        }
    }


    public class ErrorDetails
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public static class ExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionMiddleware>();
        }
    }
}

﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Utils.DependencyInjection
{
    /// <summary>
    /// Представляет расширения регистрации сервисов.
    /// </summary>
    public static class ServiceRegistratorExtensions
    {
        public static IServiceCollection AddValidators(this IServiceCollection services)
        {
            ArgumentNullException.ThrowIfNull(services);

            IReadOnlyList<Assembly> allMarkedAssemblies = MarkedAssemblyProvider.Instance.GetAllMarkedAssemblies("ValidatorsMarker");

            foreach (var markedAssembly in allMarkedAssemblies)
            {
                AddValidators(services, markedAssembly);
            }

            ValidatorOptions.Global.LanguageManager.Enabled = false;

            return services;
        }

        private static void AddValidators(IServiceCollection services, Assembly assembly)
        {
            foreach (Type implementationType in assembly
                    .ExportedTypes
                    .Where(t => t.IsClass && !t.IsAbstract))
            {
                foreach (var serviceType in implementationType
                    .GetInterfaces()
                    .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IValidator<>)))
                {
                    services.Add(new ServiceDescriptor(serviceType, implementationType, ServiceLifetime.Transient));
                }
            }
        }
    }
}

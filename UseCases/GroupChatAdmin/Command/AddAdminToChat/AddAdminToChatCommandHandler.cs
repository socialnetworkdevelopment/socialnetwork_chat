﻿using DataAccess.Interfaces;
using Entities.Enums;
using Entities;
using MediatR;
using UseCases.Exceptions;
using ApplicationServices.Interfaces;

namespace UseCases.Command.AddAdminToChat
{
    public class AddAdminToChatCommandHandler : IRequestHandler<AddAdminToChatCommand, long>
    {
        private readonly IChatReadonlyRepository _chatRepository;
        private readonly IUserReadonlyRepository _userRepository;
        private readonly IGroupChatAdminRepository _groupChatAdminRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public AddAdminToChatCommandHandler(IChatReadonlyRepository chatRepository,
            IUserReadonlyRepository userRepository,
            IGroupChatAdminRepository groupChatAdminRepository,
            IDateTimeProvider dateTimeProvider)
        {
            _chatRepository = chatRepository;
            _userRepository = userRepository;
            _groupChatAdminRepository = groupChatAdminRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<long> Handle(AddAdminToChatCommand request, CancellationToken cancellationToken)
        {
            Chat chat = await _chatRepository.FindAsync(request.ChatId)
                ?? throw new ChatMissingException(request.ChatId);

            if (chat.Type != ChatType.Group)
                throw new Exception($"Admin can be added only in group chat. Chat with Id = {request.ChatId} is {chat.Type}.");

            User? user = await _userRepository.FindAsync(request.UserId)
                ?? throw new UserMissingException(request.UserId);

            GroupChatAdmin? existChatAdmin = await _groupChatAdminRepository
                .FirstOrDefaultAsync(chatAdmin => chatAdmin.Chat.Id == request.ChatId && chatAdmin.User.Id == request.UserId);

            if (existChatAdmin != null)
                throw new Exception($"User with Id = {request.UserId} is already admin in chat with Id = {request.ChatId}.");

            GroupChatAdmin newGroupChatAdmin = new()
            {
                Chat = chat,
                User = user,
                AddedAt = _dateTimeProvider.GetNow()
            };

            GroupChatAdmin result = await _groupChatAdminRepository.AddAsync(newGroupChatAdmin);
            await _groupChatAdminRepository.SaveAsync();

            return result.Id;
        }
    }
}

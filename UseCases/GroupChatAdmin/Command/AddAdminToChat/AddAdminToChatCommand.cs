﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.AddAdminToChat
{
    public class AddAdminToChatCommand : IRequest<long>
    {
        public AddAdminToChatCommand(int chatId, long userId)
        {
            ChatId = chatId;
            UserId = userId;
        }

        public int ChatId { get; set; }

        public long UserId { get; set; }
    }
}

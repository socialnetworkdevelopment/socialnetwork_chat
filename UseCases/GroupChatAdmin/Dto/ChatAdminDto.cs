﻿namespace UseCases.Dto
{
    public class ChatAdminDto
    {
        public long Id { get; set; }

        public long UserId { get; set; }
    }
}

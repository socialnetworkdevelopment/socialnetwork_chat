﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Query.GetChatAdmins
{
    public class GetChatAdminsQuery : IRequest<IList<ChatAdminDto>>
    {
        public GetChatAdminsQuery(int chatId)
        {
            ChatId = chatId;
        }

        public int ChatId { get; set; }
    }
}

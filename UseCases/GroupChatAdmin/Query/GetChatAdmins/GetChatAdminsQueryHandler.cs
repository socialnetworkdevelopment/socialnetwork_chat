﻿using ApplicationServices.Interfaces;
using DataAccess.Interfaces;
using Entities.Enums;
using Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Command.AddAdminToChat;
using UseCases.Exceptions;
using UseCases.Dto;
using AutoMapper;

namespace UseCases.Query.GetChatAdmins
{
    public class GetChatAdminsQueryHandler : IRequestHandler<GetChatAdminsQuery, IList<ChatAdminDto>>
    {
        private readonly IChatReadonlyRepository _chatRepository;
        private readonly IGroupChatAdminReadonlyRepository _groupChatAdminRepository;
        private readonly IMapper _mapper;

        public GetChatAdminsQueryHandler(IChatReadonlyRepository chatRepository,
            IGroupChatAdminReadonlyRepository groupChatAdminRepository,
            IMapper mapper)
        {
            _chatRepository = chatRepository;
            _groupChatAdminRepository = groupChatAdminRepository;
            _mapper = mapper;
        }

        public async Task<IList<ChatAdminDto>> Handle(GetChatAdminsQuery request, CancellationToken cancellationToken)
        {
            Chat chat = await _chatRepository.FindAsync(request.ChatId)
                ?? throw new ChatMissingException(request.ChatId);

            if (chat.Type != ChatType.Group)
                throw new Exception($"Admin can be getted only from group chat. Chat with Id = {request.ChatId} is {chat.Type}.");

            IEnumerable<GroupChatAdmin> chatAdmins = await _groupChatAdminRepository
                .GetAllAsync(chatAdmin => chatAdmin.Chat.Id == request.ChatId, 
                            includeProperties: new List<string> { nameof(GroupChatAdmin.User) });

            var chatAdminsDto = _mapper.Map<List<ChatAdminDto>>(chatAdmins);

            return chatAdminsDto;
        }
    }
}

﻿using FluentValidation;
using UseCases.Command.AddAdminToChat;

namespace UseCases.Validators
{
    public class AddAdminToChatCommandValidator : AbstractValidator<AddAdminToChatCommand>
    {
        public AddAdminToChatCommandValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);

            RuleFor(e => e.ChatId)
                .GreaterThan(0);
        }
    }
}

﻿using FluentValidation;
using UseCases.Query.GetChatAdmins;

namespace UseCases.Validators
{
    public class GetChatAdminsQueryValidator : AbstractValidator<GetChatAdminsQuery>
    {
        public GetChatAdminsQueryValidator()
        {
            RuleFor(e => e.ChatId)
                .GreaterThan(0);
        }
    }
}

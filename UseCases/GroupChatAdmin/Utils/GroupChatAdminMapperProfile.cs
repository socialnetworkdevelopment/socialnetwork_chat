﻿using ApplicationServices.Interfaces;
using AutoMapper;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Dto;
using UseCases.Utils;
using Utils;

namespace UseCases.Utils
{
    public class GroupChatAdminMapperProfile : Profile
    {
        public GroupChatAdminMapperProfile()
        {
            CreateMap<GroupChatAdmin, ChatAdminDto>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id));
        }
    }

    namespace Registration
    {
        public class GroupChatAdminMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new GroupChatAdminMapperProfile());
                });
            }
        }
    }
}

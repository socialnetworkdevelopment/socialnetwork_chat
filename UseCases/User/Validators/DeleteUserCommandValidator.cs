﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);
        }
    }
}

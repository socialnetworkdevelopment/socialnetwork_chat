﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.UserId)
                .GreaterThan(0);
        }
    }
}

﻿namespace UseCases.Dto
{
    public class CreateUserDto
    {
        public long UserId { get; set; }

        public required string FirstName { get; set; }

        public required string LastName { get; set; }

        public long AvatarId { get; set; }
    }
}

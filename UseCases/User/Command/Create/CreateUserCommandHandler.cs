﻿using DataAccess.Interfaces;
using Entities;
using MediatR;

namespace UseCases.Command.Create
{
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, long>
    {
        private readonly IUserRepository _userRepository;

        public CreateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<long> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            User? user = await _userRepository.FindAsync(request.Dto.UserId);

            if (user != null)
                return user.Id;

            user = new User
            {
                Id = request.Dto.UserId,
                FirstName = request.Dto.FirstName,
                LastName = request.Dto.LastName,
                AvatarId = request.Dto.AvatarId
            };

            await _userRepository.AddAsync(user);
            await _userRepository.SaveAsync();

            return user.Id;
        }
    }
}

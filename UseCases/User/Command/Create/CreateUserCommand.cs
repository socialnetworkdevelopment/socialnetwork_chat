﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Create
{
    public class CreateUserCommand : IRequest<long>
    {
        public CreateUserCommand(CreateUserDto dto)
        {
            this.Dto = Check.NotNull(dto);
        }

        public CreateUserDto Dto { get; }
    }
}

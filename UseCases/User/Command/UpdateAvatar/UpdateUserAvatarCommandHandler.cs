﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Command.UpdateAvatar
{
    public class UpdateUserAvatarCommandHandler : IRequestHandler<UpdateUserAvatarCommand, Maybe<long>>
    {
        private readonly IUserRepository _repository;

        public UpdateUserAvatarCommandHandler(IUserRepository respository)
        {
            _repository = respository;
        }

        public async Task<Maybe<long>> Handle(UpdateUserAvatarCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _repository.FirstOrDefaultAsync(user => user.Id == request.UserId, isTracking: false);
            if (existUser == null)
                return Maybe.FromException<long>(new UserMissingException(request.UserId));

            existUser.AvatarId = request.AvatarId;

            _repository.Update(existUser);
            await _repository.SaveAsync();
            return Maybe.FromValue(existUser.Id);
        }
    }
}

﻿using MediatR;
using Utils;

namespace UseCases.Command.UpdateAvatar
{
    public class UpdateUserAvatarCommand : IRequest<Maybe<long>>
    {
        public UpdateUserAvatarCommand(long userId, long avatarId)
        {
            UserId = userId;
            AvatarId = avatarId;
        }

        public long UserId { get; }

        public long AvatarId { get; }
    }
}

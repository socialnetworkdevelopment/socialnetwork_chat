﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using Unit = MediatR.Unit;

namespace UseCases.Command.Create
{
    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand>
    {
        private readonly IUserRepository _userRepository;

        public DeleteUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _userRepository.FindAsync(request.UserId);
            if (existUser != null)
            {
                _userRepository.Remove(existUser);
                await _userRepository.SaveAsync();
            }
            return Unit.Value;
        }
    }
}

﻿using MediatR;

namespace UseCases.Command.Create
{
    public class DeleteUserCommand : IRequest
    {
        public DeleteUserCommand(long userId)
        {
            this.UserId = userId;
        }

        public long UserId { get; }
    }
}

﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateUserCommand : IRequest<Maybe<long>>
    {
        public UpdateUserCommand(UpdateUserDto dto)
        {
            this.Dto = Check.NotNull(dto);
        }

        public UpdateUserDto Dto { get; }
    }
}

﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Maybe<long>>
    {
        private readonly IUserRepository _userRepository;

        public UpdateUserCommandHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Maybe<long>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            User? existUser = await _userRepository.FindAsync(request.Dto.UserId);

            if (existUser == null)
                return Maybe.FromException<long>(new UserMissingException(request.Dto.UserId));

            existUser.FirstName = request.Dto.FirstName;
            existUser.LastName = request.Dto.LastName;

            _userRepository.Update(existUser);
            await _userRepository.SaveAsync();

            return Maybe.FromValue(existUser.Id);
        }
    }
}

﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Read
{
    public class ReadMessageCommand : IRequest<MediatR.Unit>
    {
        public ReadMessageCommand(ReadMessageDto readMessageDto)
        {
            this.Dto = Check.NotNull(readMessageDto);
        }

        public ReadMessageDto Dto { get; }
    }
}

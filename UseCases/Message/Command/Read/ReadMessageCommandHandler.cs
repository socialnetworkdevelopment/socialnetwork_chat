﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Exceptions;

namespace UseCases.Command.Read
{
    public class ReadMessageCommandHandler : IRequestHandler<ReadMessageCommand>
    {
        private readonly IMessageRepository _messageRepository;

        public ReadMessageCommandHandler(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }
        
        public async Task<MediatR.Unit> Handle(ReadMessageCommand request, CancellationToken cancellationToken)
        {
            Message? message = await _messageRepository.FirstOrDefaultAsync(message => message.Id == request.Dto.MessageId, isTracking: false);
            if (message == null)
                throw new MessageMissingException(request.Dto.MessageId);

            message.IsRead = true;
            _messageRepository.Update(message);
            await _messageRepository.SaveAsync();
            return MediatR.Unit.Value;
        }
    }
}

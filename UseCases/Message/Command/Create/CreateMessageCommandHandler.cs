﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Exceptions;

namespace UseCases.Command.Create
{
    public class CreateMessageCommandHandler : IRequestHandler<CreateMessageCommand, Message>
    {
        private readonly IMapper _mapper;
        private readonly IMessageRepository _messageRepository;
        private readonly IUserReadonlyRepository _userRepository;
        private readonly IChatReadonlyRepository _chatReadonlyRepository;

        public CreateMessageCommandHandler(IMapper mapper, IMessageRepository messageRepository,
            IUserReadonlyRepository userRepository,
            IChatReadonlyRepository chatReadonlyRepository)
        {
            _mapper = mapper;
            _messageRepository = messageRepository;
            _userRepository = userRepository;
            _chatReadonlyRepository = chatReadonlyRepository;
        }

        public async Task<Message> Handle(CreateMessageCommand request, CancellationToken cancellationToken)
        {
            Message message = _mapper.Map<Message>(request.Dto);

            User? author = await _userRepository.FindAsync(request.Dto.AuthorId)
                ?? throw new UserMissingException(request.Dto.AuthorId);
            message.Author = author;

            Chat? chat = await _chatReadonlyRepository.FirstOrDefaultAsync(chat => chat.Id == request.Dto.ChatId, 
                includeProperties: new List<string> { $"{nameof(chat.Members)}.{nameof(ChatMember.User)}" })
                ?? throw new ChatMissingException(request.Dto.ChatId);

            if (!chat.Members.Any(member => member.User.Id == author.Id))
                throw new Exception($"User with id {author.Id} doesn't have char with id {chat.Id}.");

            message.Chat = chat;

            message = await _messageRepository.AddAsync(message);
            await _messageRepository.SaveAsync();

            return message;
        }
    }
}

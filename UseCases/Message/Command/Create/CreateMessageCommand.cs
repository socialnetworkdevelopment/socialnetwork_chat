﻿using Entities;
using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Create
{
    public class CreateMessageCommand : IRequest<Message>, INotification
    {
        public CreateMessageCommand(CreateMessageDto dto)
        {
            Dto = Check.NotNull(dto);
        }

        public CreateMessageDto Dto { get; }
    }
}

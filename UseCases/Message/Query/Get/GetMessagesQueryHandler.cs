﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Dto;

namespace UseCases.Query.Get
{
    public class GetMessagesQueryHandler : IRequestHandler<GetMessagesQuery, IList<MessageDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMessageReadonlyRepository _messageRepository;

        public GetMessagesQueryHandler(IMapper mapper, IMessageReadonlyRepository messageRepository)
        {
            _mapper = mapper;
            _messageRepository = messageRepository;
        }

        public async Task<IList<MessageDto>> Handle(GetMessagesQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<Message> messages;
            if (request.PageSize == null || request.PageNumber == null)
            {
                messages = await _messageRepository.GetAllAsync(message => message.Chat.Id == request.ChatId,
                    includeProperties: new List<string> { nameof(Message.Author) });
            }
            else
            {
                messages = await _messageRepository.GetAllAsync(message => message.Chat.Id == request.ChatId,
                    includeProperties: new List<string> { nameof(Message.Author) },
                    skip: request.PageSize * (request.PageNumber - 1),
                    take: request.PageSize,
                    orderBy: messages => messages.OrderByDescending(message => message.Id));
            }

            List<MessageDto> messagesDto = _mapper.Map<List<MessageDto>>(messages); 

            return messagesDto;
        }
    }
}

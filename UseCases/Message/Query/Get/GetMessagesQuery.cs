﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Query.Get
{
    public class GetMessagesQuery : IRequest<IList<MessageDto>>
    {
        public GetMessagesQuery(int chatId, int? pageNumber, int? pageSize)
        {
            ChatId = chatId;
            PageNumber = pageNumber;
            PageSize = pageSize;
        }

        public int ChatId { get; set; }

        public int? PageNumber { get; set; }

        public int? PageSize { get; set; }
    }
}

﻿using MediatR;

namespace UseCases.Query.GetUnread
{
    public class GetUnreadMessagesCountQuery : IRequest<int>
    {
        public GetUnreadMessagesCountQuery(long forWhomUserId, int chatId)
        {
            ForWhomUserId = forWhomUserId;
            ChatId = chatId;
        }

        public long ForWhomUserId { get; set; }

        public int ChatId { get; set; }
    }
}

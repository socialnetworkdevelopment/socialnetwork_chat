﻿using DataAccess.Interfaces;
using MediatR;

namespace UseCases.Query.GetUnread
{
    public class GetUnreadMessagesCountQueryHandler : IRequestHandler<GetUnreadMessagesCountQuery, int>
    {
        private readonly IMessageReadonlyRepository _messageRepository;

        public GetUnreadMessagesCountQueryHandler(IMessageReadonlyRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public async Task<int> Handle(GetUnreadMessagesCountQuery request, CancellationToken cancellationToken)
        {
            int unreadMessagesCount = await _messageRepository
                .CountAsync(message => message.Chat.Id == request.ChatId 
                && message.Author.Id != request.ForWhomUserId 
                && !message.IsRead);
            return unreadMessagesCount;
        }
    }
}

﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Query.GetSelectively
{
    public class GetSelectivelyMessagesQuery : IRequest<IList<MessageDto>>
    {
        public GetSelectivelyMessagesQuery(GetSelectivelyMessagesDto dto)
        {
            this.Dto = dto;
        }

        public GetSelectivelyMessagesDto Dto { get; set; }
    }
}

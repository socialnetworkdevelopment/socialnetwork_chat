﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Dto;

namespace UseCases.Query.GetSelectively
{
    public class GetSelectivelyMessagesQueryHandler : IRequestHandler<GetSelectivelyMessagesQuery, IList<MessageDto>>
    {
        private readonly IMapper _mapper;
        private readonly IMessageReadonlyRepository _messageRepository;

        public GetSelectivelyMessagesQueryHandler(IMapper mapper, IMessageReadonlyRepository messageRepository)
        {
            _mapper = mapper;
            _messageRepository = messageRepository;
        }

        public async Task<IList<MessageDto>> Handle(GetSelectivelyMessagesQuery request, CancellationToken cancellationToken)
        {
            Func<IQueryable<Message>, IOrderedQueryable<Message>> orderMethod = request.Dto.Descending ?
                (query) => query.OrderByDescending(message => message.Id)
                :
                (query) => query.OrderBy(message => message.Id);

            IEnumerable<Message> messages = await _messageRepository.GetAllAsync(message => message.Chat.Id == request.Dto.ChatId,
                includeProperties: new List<string> { nameof(Message.Author) },
                take: request.Dto.TakeCount,
                skip: request.Dto.SkipCount,
                descending: request.Dto.Descending,
                orderBy: orderMethod);

            List<MessageDto> messagesDto = _mapper.Map<List<MessageDto>>(messages);

            return messagesDto;
        }
    }
}

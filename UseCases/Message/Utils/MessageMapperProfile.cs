﻿using ApplicationServices.Interfaces;
using AutoMapper;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Dto;
using Utils;

namespace UseCases.Utils
{
    public class MessageMapperProfile : Profile
    {
        public MessageMapperProfile(IDateTimeProvider dateTimeProvider)
        {
            CreateMap<CreateMessageDto, Message>()
                .ForMember(dest => dest.Chat, opt => opt.Ignore())
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => dateTimeProvider.GetNow()))
                .ForMember(dest => dest.Author, opt => opt.Ignore())
                .ForMember(dest => dest.IsRead, opt => opt.Ignore());

            CreateMap<Message, CreateMessageDto>()
                .ForMember(dest => dest.ChatId, opt => opt.MapFrom(src => src.Chat.Id))
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.Author.Id));

            CreateMap<Message, MessageDto>()
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.Author.Id))
                .ForMember(dest => dest.SendAt, opt => opt.MapFrom(src => src.CreatedAt));
        }
    }

    namespace Registration
    {
        public class MessageMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((serviceProvider, mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new MessageMapperProfile(serviceProvider.GetRequiredService<IDateTimeProvider>()));
                },
                Array.Empty<Type>());
            }
        }
    }
}

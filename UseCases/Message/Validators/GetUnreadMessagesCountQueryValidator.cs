﻿using FluentValidation;
using UseCases.Query.GetUnread;

namespace UseCases.Validators
{
    public class GetUnreadMessagesCountQueryValidator : AbstractValidator<GetUnreadMessagesCountQuery>
    {
        public GetUnreadMessagesCountQueryValidator()
        {
            RuleFor(e => e.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.ForWhomUserId)
                .GreaterThan(0);
        }
    }
}

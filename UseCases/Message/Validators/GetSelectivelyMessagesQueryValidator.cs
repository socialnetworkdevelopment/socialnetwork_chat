﻿using FluentValidation;
using UseCases.Query.GetSelectively;

namespace UseCases.Validators
{
    public class GetSelectivelyMessagesQueryValidator : AbstractValidator<GetSelectivelyMessagesQuery>
    {
        public GetSelectivelyMessagesQueryValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.TakeCount)
                .GreaterThan(0)
                .LessThan(200);

            RuleFor(e => e.Dto.SkipCount)
                .GreaterThanOrEqualTo(0);
        }
    }
}

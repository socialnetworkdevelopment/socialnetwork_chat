﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateMessageCommandValidator : AbstractValidator<CreateMessageCommand>
    {
        public CreateMessageCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.AuthorId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.Text)
                .NotEmpty()
                .MaximumLength(4096);
        }
    }
}

﻿using FluentValidation;
using UseCases.Query.Get;

namespace UseCases.Validators
{
    public class GetMessageQueryValidator : AbstractValidator<GetMessagesQuery>
    {
        public GetMessageQueryValidator()
        {
            RuleFor(e => e.ChatId)
                .GreaterThan(0);
        }
    }
}

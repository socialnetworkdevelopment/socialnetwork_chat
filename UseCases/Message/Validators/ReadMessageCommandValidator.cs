﻿using FluentValidation;
using UseCases.Command.Read;

namespace UseCases.Validators
{
    public class ReadMessageCommandValidator : AbstractValidator<ReadMessageCommand>
    {
        public ReadMessageCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.MessageId)
                .GreaterThan(0);
        }
    }
}

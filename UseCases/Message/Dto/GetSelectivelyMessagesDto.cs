﻿namespace UseCases.Dto
{
    public class GetSelectivelyMessagesDto
    {
        public int ChatId { get; set; }

        public int TakeCount { get; set; }

        public int SkipCount { get; set; }

        public bool Descending { get; set; }
    }
}

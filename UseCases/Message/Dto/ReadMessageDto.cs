﻿namespace UseCases.Dto
{
    public class ReadMessageDto
    {
        public int MessageId { get; set; }

        public int ChatId { get; set; }
    }
}

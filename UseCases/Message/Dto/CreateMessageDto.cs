﻿namespace UseCases.Dto
{
    public class CreateMessageDto
    {
        public required string Text { get; set; }

        public int AuthorId { get; set; }

        public int ChatId { get ; set; }
    }
}

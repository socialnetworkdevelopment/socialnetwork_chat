﻿using ChatNotification.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Command.Create;

namespace UseCases.NotificationHandlers
{
    public class LeavingGroupClientNotificationHandler : INotificationHandler<DeleteChatMemberCommand>
    {
        private readonly IServiceProvider _serviceProvider;

        public LeavingGroupClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(DeleteChatMemberCommand notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                await scope.ServiceProvider.GetRequiredService<IChatClientNotificator>().LeaveGroupAsync(notification.ChatId, notification.UserId);
            }
        }
    }
}

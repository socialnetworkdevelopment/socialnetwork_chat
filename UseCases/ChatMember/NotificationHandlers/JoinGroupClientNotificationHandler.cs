﻿using ChatNotification.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Command.Create;

namespace UseCases.NotificationHandlers
{
    public class JoinGroupClientNotificationHandler : INotificationHandler<CreateChatMemberCommand>
    {
        private readonly IServiceProvider _serviceProvider;

        public JoinGroupClientNotificationHandler(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task Handle(CreateChatMemberCommand notification, CancellationToken cancellationToken)
        {
            using (var scope = _serviceProvider.CreateAsyncScope())
            {
                await scope.ServiceProvider.GetRequiredService<IChatClientNotificator>()
                    .JoinGroupAsync(notification.Dto.ChatId, notification.Dto.UserId);
            }
        }
    }
}

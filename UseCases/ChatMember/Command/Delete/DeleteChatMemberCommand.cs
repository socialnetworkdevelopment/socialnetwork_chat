﻿using MediatR;


namespace UseCases.Command.Create
{
    public class DeleteChatMemberCommand : IRequest, INotification
    {
        public DeleteChatMemberCommand(int chatId, long userId)
        {
            ChatId = chatId;
            UserId = userId;
        }

        public int ChatId { get; set; }

        public long UserId { get; set; }
    }
}

﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using Unit = MediatR.Unit;

namespace UseCases.Command.Create
{
    internal class DeleteChatMemberCommandHandler : IRequestHandler<DeleteChatMemberCommand>
    {
        private readonly IChatMemberRepository _chatMemberRepository;
        private readonly IGroupChatAdminRepository _groupChatAdminRepository;

        public DeleteChatMemberCommandHandler(IMapper mapper,
            IChatMemberRepository chatMemberRepository,
            IGroupChatAdminRepository groupChatAdminRepository)
        {
            _chatMemberRepository = chatMemberRepository;
            _groupChatAdminRepository = groupChatAdminRepository;
        }

        public async Task<Unit> Handle(DeleteChatMemberCommand request, CancellationToken cancellationToken)
        {
            ChatMember? chatMember = await _chatMemberRepository.FirstOrDefaultAsync(
                chatMember => chatMember.Chat.Id == request.ChatId && chatMember.User.Id == request.UserId,
                includeProperties: new List<string>() { nameof(ChatMember.Chat), nameof(ChatMember.User) }
            );
            if (chatMember != null)
            {
                if (chatMember.Chat.Type == Entities.Enums.ChatType.Private)
                    throw new Exception("""Cannot remove chat members in chat with type "private".""");
                _chatMemberRepository.Remove(chatMember);

                GroupChatAdmin? chatAdmin = await _groupChatAdminRepository
                    .FirstOrDefaultAsync(chatAdmin => chatAdmin.Chat.Id == chatMember.Chat.Id && chatAdmin.User.Id == chatMember.User.Id);
                if (chatAdmin != null)
                    _groupChatAdminRepository.Remove(chatAdmin);

                await _chatMemberRepository.SaveAsync();
            }

            return Unit.Value;
        }
    }
}

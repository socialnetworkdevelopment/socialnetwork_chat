﻿using ApplicationServices.Interfaces.ChatMember;
using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;

namespace UseCases.Command.Create
{
    internal class CreateChatMemberCommandHandler : IRequestHandler<CreateChatMemberCommand, ChatMemberInfo>
    {
        private readonly IChatReadonlyRepository _chatRepository;
        private readonly IUserRepository _userRepository;
        private readonly IChatMemberService _chatMemberService;
        private readonly IMapper _mapper;

        public CreateChatMemberCommandHandler(
            IChatReadonlyRepository chatRepository,
            IUserRepository userRepository,
            IChatMemberService chatMemberService,
            IMapper mapper)
        {
            _chatRepository = chatRepository;
            _userRepository = userRepository;
            _chatMemberService = chatMemberService;
            _mapper = mapper;
        }

        public async Task<ChatMemberInfo> Handle(CreateChatMemberCommand request, CancellationToken cancellationToken)
        {
            Chat? chat = await _chatRepository.FirstOrDefaultAsync(
                chat => chat.Id == request.Dto.ChatId);

            if (chat == null)
                throw new ChatMissingException(request.Dto.ChatId);
            if (chat.Type == Entities.Enums.ChatType.Private)
                throw new Exception("""Cannot add chat members in chat with type "private".""");

            User? user = await _userRepository.FindAsync(request.Dto.UserId)
                ?? throw new UserMissingException(request.Dto.UserId);

            ChatMember chatMember = await _chatMemberService.CreateAsync(chat, user);

            return _mapper.Map<ChatMemberInfo>(chatMember);
        }
    }
}

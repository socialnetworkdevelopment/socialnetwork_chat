﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Create
{
    public class CreateChatMemberCommand : IRequest<ChatMemberInfo>, INotification
    {
        public CreateChatMemberCommand(CreateChatMemberDto dto)
        {
            this.Dto = Check.NotNull(dto);
        }

        public CreateChatMemberDto Dto { get; set; }
    }
}

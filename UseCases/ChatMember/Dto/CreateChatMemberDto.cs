﻿namespace UseCases.Dto
{
    public class CreateChatMemberDto
    { 
        public long UserId { get; set; }

        public int ChatId { get; set; }
    }
}

﻿namespace UseCases.Dto
{
    public class DeleteChatMemberDto
    {
        public int UserId { get; set; }

        public int ChatId { get; set; }
    }
}

﻿using AutoMapper;
using Entities;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Dto;
using Utils;

namespace UseCases.Utils
{
    public class ChatMemberMapperProfile : Profile
    {
        public ChatMemberMapperProfile()
        {
            CreateMap<CreateChatDto, Chat>()
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore());
            CreateMap<Chat, CreateChatDto>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy.Id));
            CreateMap<UpdateChatDto, Chat>();
        }
    }

    namespace Registration
    {
        public class ChatMemberMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new ChatMemberMapperProfile());
                });
            }
        }
    }
}

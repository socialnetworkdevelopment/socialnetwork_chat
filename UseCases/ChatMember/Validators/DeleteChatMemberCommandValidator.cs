﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class DeleteChatMemberCommandValidator : AbstractValidator<DeleteChatMemberCommand>
    {
        public DeleteChatMemberCommandValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);

            RuleFor(e => e.ChatId)
                .GreaterThan(0);
        }
    }
}

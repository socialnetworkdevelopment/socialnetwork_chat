﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateChatMemberCommandValidator : AbstractValidator<CreateChatMemberCommand>
    {
        public CreateChatMemberCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.UserId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.ChatId)
                .GreaterThan(0);
        }
    }
}

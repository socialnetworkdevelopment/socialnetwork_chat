﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace UseCases.Registration
{
    public class UseCaseHandlersRegistrator : IServiceRegistrator
    {
        public void AddServices(IServiceCollection services)
        {
            Check.NotNull(services).AddMediatR(typeof(AssemblyMarker).Assembly);
        }
    }
}

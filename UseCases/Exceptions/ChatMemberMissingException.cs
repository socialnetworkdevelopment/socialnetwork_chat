﻿using Utils;

namespace UseCases.Exceptions
{
    public class ChatMemberMissingException : ValueMissingException
    {
        public ChatMemberMissingException(int chatID)
        {
            ChatID = chatID;
        }

        public int ChatID { get; }

        protected override string CreateMessage()
        {
            return $"Failed to get the chatMember by id = {ChatID}.";
        }
    }
}

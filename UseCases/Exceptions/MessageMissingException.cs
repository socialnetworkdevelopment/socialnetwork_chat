﻿using Utils;

namespace UseCases.Exceptions
{
    public class MessageMissingException : ValueMissingException
    {
        public MessageMissingException(long messageId)
        {
            this.MessageId = messageId;
        }

        public long MessageId { get; }

        protected override string CreateMessage()
        {
            return $"Failed to get the message by id = {this.MessageId}.";
        }
    }
}

﻿using Utils;

namespace UseCases.Exceptions
{
    public class UserMissingException : ValueMissingException
    {
        public UserMissingException(long userId)
        {
            this.UserId = userId;
        }

        public long UserId { get; }

        protected override string CreateMessage()
        {
            return $"Failed to get the user by id = {UserId}.";
        }
    }
}

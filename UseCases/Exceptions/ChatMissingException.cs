﻿using Utils;

namespace UseCases.Exceptions
{
    public class ChatMissingException : ValueMissingException
    {
        public ChatMissingException(int chatID)
        {
            this.ChatID = chatID;
        }

        public ChatMissingException(string message)
        {
            Message = message;
        }

        public int ChatID { get; }

        public string Message { get; }

        protected override string CreateMessage()
        {
            return string.IsNullOrEmpty(Message) ? $"Failed to get the chat by id = {ChatID}." : Message;
        }
    }
}

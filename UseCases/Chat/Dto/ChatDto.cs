﻿using Entities.Enums;

namespace UseCases.Dto
{
    public class ChatDto
    {
        public int Id { get; set; }
        
        public string? Name { get; set; }

        public long? ImgId { get; set; }

        public int CreatedBy { get; set; }

        public required List<ChatMemberInfo> ChatMembers { get; set; }

        public ChatType Type { get; set; }
    }

    public class ChatMemberInfo
    {
        public int Id { get; set; }

        public double UserId { get; set; }

        public required string FirstName { get; set; }

        public required string LastName { get; set; }

        public long? AvatarId { get; set; }
    }
}

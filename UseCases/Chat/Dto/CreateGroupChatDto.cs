﻿namespace UseCases.Dto
{
    public class CreateGroupChatDto
    {
        public required string Name { get; set; }

        public long? ImgId { get; set; }

        public int CreatedBy { get; set; }

        public required List<long> ChatMembers { get; set; }
    }
}

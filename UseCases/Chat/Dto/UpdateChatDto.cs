﻿namespace UseCases.Dto
{
    public class UpdateChatDto
    {
        public int Id { get; set; }

        public string? Name { get; set; }

        public long? ImgId { get; set; }
    }
}

﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using LanguageExt;
using MediatR;
using UseCases.Dto;

namespace UseCases.Query.GetByUserId
{
    public class GetChatByUserIdQueryHandler : IRequestHandler<GetChatByUserIdQuery, IList<ChatDto>>
    {
        private readonly IMapper _mapper;
        private readonly IChatMemberReadonlyRepository _chatMemberRepository;
        private readonly IChatReadonlyRepository _chatRepository;

        public GetChatByUserIdQueryHandler(IMapper mapper, IChatMemberReadonlyRepository chatMemberRepository,
            IChatReadonlyRepository chatRepository)
        {
            _mapper = mapper;
            _chatMemberRepository = chatMemberRepository;
            _chatRepository = chatRepository;
        }

        public async Task<IList<ChatDto>> Handle(GetChatByUserIdQuery request, CancellationToken cancellationToken)
        {
            IEnumerable<ChatMember> chatMembersByUser = await _chatMemberRepository.GetAllAsync(
                chatMember => chatMember.User.Id == request.UserId,
                includeProperties: new List<string> 
                { 
                    nameof(ChatMember.Chat), 
                    nameof(ChatMember.User), 
                    $"{nameof(Chat)}.{nameof(Chat.Members)}",
                    $"{nameof(Chat)}.{nameof(Chat.Members)}.{nameof(ChatMember.User)}"
                }
            );

            List<Chat> chatsByUser = chatMembersByUser
                .Select(chatMember => chatMember.Chat)
                .Distinct()
                .ToList();

            List<ChatDto> chatsDto = chatsByUser
                .Select(_mapper.Map<ChatDto>)
                .ToList();

            return chatsDto;
        }
    }
}

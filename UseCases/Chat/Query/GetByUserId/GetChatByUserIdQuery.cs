﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Query.GetByUserId
{
    public class GetChatByUserIdQuery : IRequest<IList<ChatDto>>
    {
        public GetChatByUserIdQuery(long userId)
        {
            this.UserId = userId;
        }

        public long UserId { get; set; }
    }
}

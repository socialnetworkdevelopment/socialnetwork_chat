﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Query.Get
{
    public class GetChatQueryHandler : IRequestHandler<GetChatQuery, Maybe<ChatDto>>
    {
        private readonly IMapper _mapper;
        private readonly IChatReadonlyRepository _repository;

        public GetChatQueryHandler(IMapper mapper, IChatReadonlyRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Maybe<ChatDto>> Handle(GetChatQuery request, CancellationToken cancellationToken)
        {
            Chat? chat = 
                await _repository.FirstOrDefaultAsync(
                    chat => chat.Id == request.Id, 
                    includeProperties: new List<string>() 
                    { 
                        nameof(chat.CreatedBy), 
                        nameof(chat.Members),
                        $"{nameof(Chat.Members)}.{nameof(ChatMember.User)}"
                    }
                );
            if (chat == null)
                return Maybe.FromException<ChatDto>(new ChatMissingException(request.Id));

            return Maybe.FromValue(_mapper.Map<ChatDto>(chat));
        }
    }
}

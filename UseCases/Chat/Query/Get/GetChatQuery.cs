﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.Get
{
    public class GetChatQuery : IRequest<Maybe<ChatDto>>
    {
        public GetChatQuery(int id)
        {
            this.Id = id;
        }

        public int Id { get; set; }
    }
}

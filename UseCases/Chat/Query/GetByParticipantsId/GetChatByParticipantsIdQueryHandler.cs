﻿using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using Entities.Enums;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Query.GetByParticipantsId
{
    public class GetChatByParticipantsIdQueryHandler : IRequestHandler<GetChatByParticipantsIdQuery, Maybe<ChatDto>>
    {
        private readonly IMapper _mapper;
        private readonly IChatReadonlyRepository _repository;

        public GetChatByParticipantsIdQueryHandler(IMapper mapper, IChatReadonlyRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<Maybe<ChatDto>> Handle(GetChatByParticipantsIdQuery request, CancellationToken cancellationToken)
        {
            Chat? chat =
                await _repository.FirstOrDefaultAsync(
                    chat => chat.Type == ChatType.Private && 
                            chat.Members.All(chatMember => chatMember.User.Id == request.ParticipantUserId1 || chatMember.User.Id == request.ParticipantUserId2),
                    includeProperties: new List<string>()
                    {
                        nameof(chat.Members),
                        $"{nameof(Chat.Members)}.{nameof(ChatMember.User)}"
                    }
                );
            if (chat == null)
                return Maybe.FromException<ChatDto>(
                    new ChatMissingException($"Failed to get the chat by chat members with id: {request.ParticipantUserId1}, {request.ParticipantUserId2} "));

            return Maybe.FromValue(_mapper.Map<ChatDto>(chat));
        }
    }
}

﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Query.GetByParticipantsId
{
    public class GetChatByParticipantsIdQuery : IRequest<Maybe<ChatDto>>
    {
        public GetChatByParticipantsIdQuery(long participantUserId1, long participantUserId2)
        {
            ParticipantUserId1 = participantUserId1;
            ParticipantUserId2 = participantUserId2;
        }

        public long ParticipantUserId1 { get; set; }

        public long ParticipantUserId2 { get; set; }
    }
}

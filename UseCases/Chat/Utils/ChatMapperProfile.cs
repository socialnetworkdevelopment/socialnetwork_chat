﻿using ApplicationServices.Interfaces;
using AutoMapper;
using Entities;
using Entities.Enums;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Dto;
using Utils;

namespace UseCases.Utils
{
    public class ChatMapperProfile : Profile
    {       
        public ChatMapperProfile(IDateTimeProvider dateTimeProvider)
        {
            CreateMap<CreateChatDto, Chat>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => ChatType.Private))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => dateTimeProvider.GetNow()))
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore());

            CreateMap<CreateGroupChatDto, Chat>()
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => ChatType.Group))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => dateTimeProvider.GetNow()))
                .ForMember(dest => dest.CreatedBy, opt => opt.Ignore());

            CreateMap<Chat, CreateChatDto>()
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy.Id));

            CreateMap<UpdateChatDto, Chat>();

            CreateMap<Chat, ChatDto>()
                .ForMember(dest => dest.ChatMembers, opt => opt.MapFrom(src => 
                    src.Members.Select(member => 
                    new ChatMemberInfo 
                    { 
                        Id = member.Id, 
                        UserId = member.User.Id, 
                        FirstName = member.User.FirstName,
                        LastName = member.User.LastName,
                        AvatarId = member.User.AvatarId
                    })))
                .ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy.Id));

            CreateMap<ChatMember, ChatMemberInfo>()
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.User.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.User.LastName))
                .ForMember(dest => dest.AvatarId, opt => opt.MapFrom(src => src.User.AvatarId));
        }
    }

    namespace Registration
    {
        public class ChatMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((serviceProvider, mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new ChatMapperProfile(serviceProvider.GetRequiredService<IDateTimeProvider>()));
                },
                Array.Empty<Type>());
            }
        }
    }
}

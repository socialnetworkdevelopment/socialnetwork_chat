﻿using ApplicationServices.Interfaces.ChatMember;
using AutoMapper;
using DataAccess.Interfaces;
using Entities;
using Entities.Enums;
using MediatR;
using UseCases.Dto;
using UseCases.Exceptions;

namespace UseCases.Command.Create
{
    public class CreateChatCommandHandler : IRequestHandler<CreateChatCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IChatRepository _chatRepository;
        private readonly IUserReadonlyRepository _userRepository;
        private readonly IChatMemberService _chatMemberService;

        public CreateChatCommandHandler(IMapper mapper, IChatRepository chatRepository, 
            IUserReadonlyRepository userRepository,
            IChatMemberService chatMemberService)
        {
            _mapper = mapper;
            _chatRepository = chatRepository;
            _userRepository = userRepository;
            _chatMemberService = chatMemberService;
        }

        public async Task<int> Handle(CreateChatCommand request, CancellationToken cancellationToken)
        {
            Chat? existChet = await _chatRepository.FirstOrDefaultAsync(chat =>
                chat.Type == ChatType.Private &&
                chat.Members.All(chatMember => chatMember.User.Id == request.Dto.CreatedBy || chatMember.User.Id == request.Dto.СompanionId));

            if (existChet != null)
                throw new InvalidOperationException($"Private chat with users {request.Dto.CreatedBy} and {request.Dto.СompanionId} already exist.");

            Chat chatForInsert = _mapper.Map<CreateChatDto, Chat>(request.Dto);

            chatForInsert.CreatedBy = await this.GetUserAsync(request.Dto.CreatedBy);

            Chat newChat = await _chatRepository.AddAsync(chatForInsert);
            await _chatRepository.SaveAsync();

            try
            {
                await _chatMemberService.CreateAsync(newChat, chatForInsert.CreatedBy);
                User companion =  await this.GetUserAsync(request.Dto.СompanionId);
                await _chatMemberService.CreateAsync(newChat, companion);
            }
            catch
            {
                _chatRepository.Remove(newChat);
                await _chatRepository.SaveAsync();
                throw;
            }
            
            return newChat.Id;
        }

        private async Task<User> GetUserAsync(int userId) => await _userRepository.FindAsync(userId)
                ?? throw new UserMissingException(userId);
    }
}

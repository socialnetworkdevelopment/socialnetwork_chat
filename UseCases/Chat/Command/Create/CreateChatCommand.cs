﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.Create
{
    public class CreateChatCommand : IRequest<int>
    {
        public CreateChatCommand(CreateChatDto dto)
        {
            Dto = dto ?? throw new ArgumentNullException(nameof(dto));
        }

        public CreateChatDto Dto { get; }
    }
}

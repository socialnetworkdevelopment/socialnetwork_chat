﻿using AutoMapper;
using Entities;
using UseCases.Exceptions;
using MediatR;
using UseCases.Dto;
using UseCases.Command.Create;
using DataAccess.Interfaces;
using ApplicationServices.Interfaces;

namespace UseCases.Command.CreateGroupChat
{
    public class CreateGroupChatCommandHandler : IRequestHandler<CreateGroupChatCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IChatRepository _chatRepository;
        private readonly IUserReadonlyRepository _userRepository;
        private readonly IMediator _sender;
        private readonly IGroupChatAdminRepository _chatAdminRepository;
        private readonly IDateTimeProvider _dateTimeProvider;

        public CreateGroupChatCommandHandler(IMapper mapper, IChatRepository chatRepository,
            IUserReadonlyRepository userRepository,
            IMediator sender,
            IGroupChatAdminRepository chatAdminRepository,
            IDateTimeProvider dateTimeProvider)
        {
            _mapper = mapper;
            _chatRepository = chatRepository;
            _userRepository = userRepository;
            _sender = sender;
            _chatAdminRepository = chatAdminRepository;
            _dateTimeProvider = dateTimeProvider;
        }

        public async Task<int> Handle(CreateGroupChatCommand request, CancellationToken cancellationToken)
        {
            var chatCreator = await _userRepository.FindAsync(request.Dto.CreatedBy)
                ?? throw new UserMissingException(request.Dto.CreatedBy);

            Chat chatForInsert = _mapper.Map<CreateGroupChatDto, Chat>(request.Dto);
            chatForInsert.CreatedBy = chatCreator;

            Chat newChat = await _chatRepository.AddAsync(chatForInsert);
            await _chatRepository.SaveAsync();

            try
            {
                if (!request.Dto.ChatMembers.Contains(chatCreator.Id))
                    await this.CreateChatMemberAsync(newChat, chatCreator.Id);

                foreach (var chatMemberId in request.Dto.ChatMembers)
                   await this.CreateChatMemberAsync(newChat, chatMemberId);

                GroupChatAdmin groupChatAdmin = new()
                {
                    Chat = newChat,
                    User = chatCreator,
                    AddedAt = _dateTimeProvider.GetNow()
                };
                await _chatAdminRepository.AddAsync(groupChatAdmin);
                await _chatAdminRepository.SaveAsync();
            }
            catch
            {
                _chatRepository.Remove(newChat);
                await _chatRepository.SaveAsync();
                throw;
            }
            return newChat.Id;
        }

        private async Task CreateChatMemberAsync(Chat newChat, long chatMemberId)
        {
            var createGroupChatMemberDto = new CreateChatMemberDto { ChatId = newChat.Id, UserId = chatMemberId };
            var createGroupChatMemberCommand = new CreateChatMemberCommand(createGroupChatMemberDto);
            await _sender.Send(createGroupChatMemberCommand);
        }
    }
}

﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.CreateGroupChat
{
    public class CreateGroupChatCommand : IRequest<int>
    {
        public CreateGroupChatCommand(CreateGroupChatDto dto)
        {
            Dto = Check.NotNull(dto);
        }

        public CreateGroupChatDto Dto { get; }
    }
}

﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using Unit = MediatR.Unit;

namespace UseCases.Command.Delete
{
    public class DeleteChatCommandHandler : IRequestHandler<DeleteChatCommand>
    {
        private readonly IChatRepository _repository;

        public DeleteChatCommandHandler(IChatRepository respository)
        {
            _repository = respository;
        }

        public async Task<Unit> Handle(DeleteChatCommand request, CancellationToken cancellationToken)
         {
            Chat? exist = await _repository.FindAsync(request.Id);
            if (exist != null)
            {
                _repository.Remove(exist);
                await _repository.SaveAsync();
            }
            return Unit.Value;
        }
    }
}

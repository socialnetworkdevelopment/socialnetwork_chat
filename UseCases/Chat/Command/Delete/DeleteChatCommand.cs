﻿using MediatR;
using Utils;

namespace UseCases.Command.Delete
{
    public class DeleteChatCommand : IRequest
    {
        public DeleteChatCommand(int id)
        {
            this.Id = Check.IsPositive(id);
        }

        public int Id { get; }
    }
}

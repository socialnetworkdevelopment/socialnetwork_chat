﻿using MediatR;
using UseCases.Dto;

namespace UseCases.Command.AddMemberToGroupChat
{
    public class AddMemberToGroupChatCommand : IRequest<List<ChatMemberInfo>>
    {
        public AddMemberToGroupChatCommand(int chatId, List<long> userIds)
        {
            ChatId = chatId;
            UserIds = userIds; 
        }

        public int ChatId { get; set; }

        public List<long> UserIds { get; set; }
    }
}

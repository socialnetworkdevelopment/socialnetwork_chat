﻿using AutoMapper;
using MediatR;
using UseCases.Dto;
using UseCases.Command.Create;

namespace UseCases.Command.AddMemberToGroupChat
{
    public class AddMemberToGroupChatCommandHandler : IRequestHandler<AddMemberToGroupChatCommand, List<ChatMemberInfo>>
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public AddMemberToGroupChatCommandHandler(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        public async Task<List<ChatMemberInfo>> Handle(AddMemberToGroupChatCommand request, CancellationToken cancellationToken)
        {
            List<ChatMemberInfo> addedUsers = new();
            foreach (var userId in request.UserIds)
            {
                CreateChatMemberCommand createChatMemberCommand = new(new CreateChatMemberDto
                {
                    ChatId = request.ChatId,
                    UserId = userId,
                });
                var chatMemberInfo = await _sender.Send(createChatMemberCommand, cancellationToken);
                addedUsers.Add(chatMemberInfo);
            }
            return addedUsers;
        }
    }
}

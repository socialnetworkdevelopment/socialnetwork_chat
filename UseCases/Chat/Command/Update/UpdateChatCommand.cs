﻿using MediatR;
using UseCases.Dto;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateChatCommand : IRequest<Maybe<int>>
    {
        public UpdateChatCommand(UpdateChatDto chatDto)
        {
            this.Dto = Check.NotNull(chatDto);
        }

        public UpdateChatDto Dto { get; }
    }
}

﻿using DataAccess.Interfaces;
using Entities;
using MediatR;
using UseCases.Exceptions;
using Utils;

namespace UseCases.Command.Update
{
    public class UpdateChatCommandHandler : IRequestHandler<UpdateChatCommand, Maybe<int>>
    {
        private readonly IChatRepository _repository;

        public UpdateChatCommandHandler(IChatRepository respository)
        {
            _repository = respository;
        }

        public async Task<Maybe<int>> Handle(UpdateChatCommand request, CancellationToken cancellationToken)
        {
            Chat? existChat = await _repository.FirstOrDefaultAsync(chat => chat.Id == request.Dto.Id, isTracking: false);
            if (existChat == null)
                return Maybe.FromException<int>(new ChatMissingException(request.Dto.Id));

            if (existChat.Type == Entities.Enums.ChatType.Private)
                throw new InvalidOperationException("Private chat cannot be updated.");

            if (request.Dto.ImgId != null)
                existChat.ImgId = request.Dto.ImgId;
            if (request.Dto.Name != null)
                existChat.Name = request.Dto.Name;

            _repository.Update(existChat);
            await _repository.SaveAsync();
            return Maybe.FromValue(existChat.Id);
        }
    }
}

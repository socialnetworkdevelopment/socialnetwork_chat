﻿using FluentValidation;
using UseCases.Query.GetByUserId;

namespace UseCases.Validators
{
    public class GetChatByUserIdCommandValidator : AbstractValidator<GetChatByUserIdQuery>
    {
        public GetChatByUserIdCommandValidator()
        {
            RuleFor(e => e.UserId)
                .GreaterThan(0);
        }
    }
}

﻿using FluentValidation;
using UseCases.Query.Get;

namespace UseCases.Validators
{
    public class GetChatCommandValidator : AbstractValidator<GetChatQuery>
    {
        public GetChatCommandValidator()
        {
            RuleFor(e => e.Id)
                .GreaterThan(0);
        }
    }
}

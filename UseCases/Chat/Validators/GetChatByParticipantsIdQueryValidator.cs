﻿using FluentValidation;
using FluentValidation.Validators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCases.Command.Update;
using UseCases.Query.GetByParticipantsId;

namespace UseCases.Validators
{
    public class GetChatByParticipantsIdQueryValidator : AbstractValidator<GetChatByParticipantsIdQuery>
    {
        public GetChatByParticipantsIdQueryValidator()
        {
            RuleFor(e => e.ParticipantUserId1)
                .GreaterThan(0);

            RuleFor(e => e.ParticipantUserId2)
                .GreaterThan(0);

            RuleFor(e => e.ParticipantUserId2)
                .NotEqual(e => e.ParticipantUserId1)
                .WithMessage("ParticipantUserId1 cannot be equal to ParticipantUserId2");
        }
    }
}

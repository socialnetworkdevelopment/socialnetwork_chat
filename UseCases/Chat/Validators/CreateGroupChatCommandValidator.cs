﻿using FluentValidation;
using UseCases.Command.CreateGroupChat;

namespace UseCases.Validators
{
    public class CreateGroupChatCommandValidator : AbstractValidator<CreateGroupChatCommand>
    {
        public CreateGroupChatCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.Name)
                .NotEmpty()
                .MaximumLength(100);

            RuleFor(e => e.Dto.CreatedBy)
                .GreaterThan(0);

            RuleFor(e => e.Dto.ChatMembers)
                .Must(chatMembers => chatMembers.Count > 0).WithMessage("Chat members collection cannot be empty.");

            RuleForEach(e => e.Dto.ChatMembers)
                .GreaterThan(0);
        }
    }
}

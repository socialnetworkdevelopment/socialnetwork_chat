﻿using FluentValidation;
using UseCases.Command.Delete;

namespace UseCases.Validators
{
    public class DeleteChatCommandValidator : AbstractValidator<DeleteChatCommand>
    {
        public DeleteChatCommandValidator()
        {
            RuleFor(e => e.Id)
                .GreaterThan(0);
        }
    }
}

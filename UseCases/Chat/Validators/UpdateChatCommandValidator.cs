﻿using FluentValidation;
using UseCases.Command.Update;

namespace UseCases.Validators
{
    public class UpdateChatCommandValidator : AbstractValidator<UpdateChatCommand>
    {
        public UpdateChatCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.Id)
                .GreaterThan(0);

            RuleFor(e => e.Dto.Name)
                .NotEmpty()
                .MaximumLength(100)
                .When(e => e.Dto.ImgId == null);

            RuleFor(e => e.Dto.ImgId)
                .NotEmpty()
                .When(e => string.IsNullOrEmpty(e.Dto.Name));
        }
    }
}

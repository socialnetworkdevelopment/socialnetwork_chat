﻿using FluentValidation;
using UseCases.Command.Create;

namespace UseCases.Validators
{
    public class CreateChatCommandValidator : AbstractValidator<CreateChatCommand>
    {
        public CreateChatCommandValidator()
        {
            RuleFor(e => e.Dto)
                .NotNull();

            RuleFor(e => e.Dto.СompanionId)
                .GreaterThan(0);

            RuleFor(e => e.Dto.CreatedBy)
                .GreaterThan(0);
        }
    }
}

﻿using ApplicationServices.Interfaces.CurrentUser;
using ChatNotification.Interfaces;
using DataAccess.Interfaces;
using Entities;
using Hub.SignalR.Implementation;
using Hub.SignalR.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using UseCases.Exceptions;
using Utils;

namespace Hub.SignalR
{
    public class ChatClientNotificator : IChatClientNotificator
    {
        private readonly IHubContext<ChatHub, IChatHub> _chatHub;
        private readonly IGroupNameProvider _groupNameProvider;
        private readonly ICurrentUserProvider<long> _currentUserProvider;
        private readonly IConnectionsMappingService _connectionsMappingService;
        private readonly IChatReadonlyRepository _chatReadonlyRepository;

        public ChatClientNotificator(
            IHubContext<ChatHub, IChatHub> chatHub, 
            IGroupNameProvider groupNameProvider, 
            ICurrentUserProvider<long> currentUserProvider,
            IConnectionsMappingService connectionsMappingService,
            IServiceProvider serviceProvider,
            IChatReadonlyRepository chatReadonlyRepository)
        {
            _chatHub = chatHub;
            _groupNameProvider = groupNameProvider;
            _currentUserProvider = currentUserProvider;
            _connectionsMappingService = connectionsMappingService;
            _chatReadonlyRepository = chatReadonlyRepository;
        }

        public async Task SendMessageAsync(User author, int chatId, long messageId, string message, DateTime sendAt)
        {
            await _chatHub.Clients.Group(_groupNameProvider.GetGroupName(chatId))
                .ReceiveMessage(author, chatId, messageId, message, sendAt);
        }

        public async Task ReadMessageAsync(long messageId, int chatId)
        {
            Chat chat = await _chatReadonlyRepository.FirstOrDefaultAsync(chat => chat.Id == chatId,
                includeProperties: new List<string>() { nameof(Chat.Members), $"{nameof(Chat.Members)}.{nameof(ChatMember.User)}" })
                ?? throw new ChatMissingException(chatId);

            var currentUserId = _currentUserProvider.GetCurrentUserId();
            foreach (ChatMember chatMember in chat.Members)
            {
                if (chatMember.User.Id == currentUserId)
                    continue;

                await _chatHub.Clients.User(chatMember.User.Id.ToString())
                    .ReadMessage(messageId, chatId);
            }
           
        }

        public async Task JoinGroupAsync(int chatId, long userId)
        {
            string groupName = _groupNameProvider.GetGroupName(chatId);
            await _chatHub.Clients.Group(groupName).JoinGroup(userId, chatId);

            IEnumerable<string> connectionIDsForUser = _connectionsMappingService.GetConnections(userId);
            foreach (string connectionId in connectionIDsForUser)
            {
                await _chatHub.Groups.AddToGroupAsync(connectionId, groupName);
            }
        }

        public async Task LeaveGroupAsync(int chatId, long userId)
        {
            string groupName = _groupNameProvider.GetGroupName(chatId);
            await _chatHub.Clients.Group(groupName).LeaveGroup(userId, chatId);

            IEnumerable<string> connectionIDsForUser = _connectionsMappingService.GetConnections(userId);
            foreach (string connectionId in connectionIDsForUser)
            {
                await _chatHub.Groups.RemoveFromGroupAsync(connectionId, groupName);
            }
        }
    }

    namespace Registration
    {
        public class ChatClientNotificatorRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services)
                    .AddScoped<IChatClientNotificator, ChatClientNotificator>();
            }
        }
    }
}

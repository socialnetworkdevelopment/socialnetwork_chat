﻿using Hub.SignalR.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Hub.SignalR
{
    public class GroupNameProvider : IGroupNameProvider
    {
        public string GetGroupName(int chatId) => $"chat{chatId}";
    }

    namespace Registration
    {
        public class GroupNameProviderRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddSingleton<IGroupNameProvider, GroupNameProvider>();
            }
        }
    }
}

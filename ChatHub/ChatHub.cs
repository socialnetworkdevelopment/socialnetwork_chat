﻿using Hub.SignalR.Interfaces;
using Hub.SignalR.Validation;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using SignalRSwaggerGen.Attributes;
using UseCases.Dto;
using UseCases.Query.GetByUserId;
using Utils;

namespace Hub.SignalR.Implementation
{
    [Authorize]
    [SignalRHub]
    public class ChatHub : Hub<IChatHub>
    {
        private readonly IMediator _sender;
        private readonly IConnectionsMappingService _connectionsMappingService;
        private readonly IGroupNameProvider _groupNameProvider;

        public ChatHub(
            IMediator sender, 
            IConnectionsMappingService connectionsMappingService,
            IGroupNameProvider groupNameProvider)
        {
            _sender = sender;
            _connectionsMappingService = connectionsMappingService;
            _groupNameProvider = groupNameProvider;
        }

        public async override Task OnConnectedAsync()
        {
            if (!int.TryParse(this.Context.UserIdentifier, out int currentUserId))
                throw new Exception("UserIdentifier is not integer.");

            if (!_connectionsMappingService.GetConnections(currentUserId).Contains(Context.ConnectionId))
            {
                _connectionsMappingService.Add(currentUserId, Context.ConnectionId);
            }

            GetChatByUserIdQuery getChatByUserIdCommand = new(currentUserId);
            IList<ChatDto> userChats = await _sender.Send(getChatByUserIdCommand);
            foreach (ChatDto userChat in userChats)
            {
                await this.Groups.AddToGroupAsync(this.Context.ConnectionId, _groupNameProvider.GetGroupName(userChat.Id));
            }

            await base.OnConnectedAsync();
        }

        public async override Task OnDisconnectedAsync(Exception? exception)
        {
            if (!int.TryParse(this.Context.UserIdentifier, out int currentUserId))
                throw new Exception("UserIdentifier is not integer.");

            _connectionsMappingService.Remove(currentUserId, Context.ConnectionId);

            await base.OnDisconnectedAsync(exception);
        }
    }

    namespace Registration
    {
        public class ChatHubRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services)
                    .AddSignalR(options =>
                    {
                        options.AddFilter<ValidationFilter>();
                        options.EnableDetailedErrors = true;
                    });
            }
        }
    }
}
﻿namespace Hub.SignalR.Interfaces
{
    public interface IGroupNameProvider
    {
        string GetGroupName(int chatId);
    }
}

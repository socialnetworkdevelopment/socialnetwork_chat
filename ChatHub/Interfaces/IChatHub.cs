﻿using Entities;

namespace Hub.SignalR.Interfaces
{
    public interface IChatHub
    {
        Task ReceiveMessage(User author, int chatId, long messageId, string message, DateTime sendAt);

        Task JoinGroup(long userId, int groupId);

        Task LeaveGroup(long userId, int groupId);

        Task ReadMessage(long messageId, int chatId);
    }
}

﻿using System.Collections.Concurrent;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;

namespace Hub.SignalR.Validation
{
    public class ValidationFilter : IHubFilter
    {
        private readonly ConcurrentDictionary<string, IEnumerable<IValidateHubMethodInvocation>> _methodInvocationCache = new();

        async ValueTask<object> InvokeMethodAsync(HubInvocationContext invocationContext, Func<HubInvocationContext, ValueTask<object>> next)
        {
            var methodLevelValidator = _methodInvocationCache.GetOrAdd(invocationContext.HubMethodName,
                methodName => invocationContext.HubMethod.CustomAttributes.OfType<IValidateHubMethodInvocation>()).FirstOrDefault();

            if (methodLevelValidator == null)
                return next(invocationContext);

            var validationErrors = methodLevelValidator.ValidateHubMethodInvocation(invocationContext);

            if (validationErrors.Count() == 0)
                return next(invocationContext);

            string errorsInJson = JsonConvert.SerializeObject(validationErrors);

            // Send error back to the client
            return FromError<object>(new ValidationException(String.Format("ValidationError|{0}.", errorsInJson)));
        }

        private static Task<T> FromError<T>(Exception e)
        {
            var tcs = new TaskCompletionSource<T>();
            tcs.SetException(e);
            return tcs.Task;
        }
    }
}

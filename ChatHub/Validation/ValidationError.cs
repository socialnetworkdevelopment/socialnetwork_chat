﻿namespace Hub.SignalR.Validation
{
    public class ValidationError
    {
        public string ParamName { get; set; }

        public string EntityType { get; set; }

        public List<string> Errors { get; set; }
    }
}

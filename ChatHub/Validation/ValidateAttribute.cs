﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.SignalR;

namespace Hub.SignalR.Validation
{
    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public sealed class ValidateAttribute : Attribute, IValidateHubMethodInvocation
    {
        public IEnumerable<ValidationError> ValidateHubMethodInvocation(HubInvocationContext hubIncomingInvokerContext)
        {
            var errors = new List<ValidationError>();

            for (int i = 0; i < hubIncomingInvokerContext.HubMethodArguments.Count; ++i)
            {
                object? arg = hubIncomingInvokerContext.HubMethodArguments[i];
                var validationContext = new ValidationContext(arg);
                var results = new List<ValidationResult>();

                bool valid = Validator.TryValidateObject(arg, validationContext, results, true);

                if (!valid)
                {
                    System.Reflection.ParameterInfo parameter = hubIncomingInvokerContext.HubMethod.GetParameters()[i];

                    ValidationError ve = new()
                    {
                        ParamName = parameter.Name,
                        Errors = results.Select(validationResult => validationResult.ErrorMessage).ToList()
                    };
                    errors.Add(ve);
                }
            }

            return errors;
        }
    }
}

﻿using Hub.SignalR.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Hub.SignalR.Connections
{
    public class ConnectionsMappingService : IConnectionsMappingService
    {
        private readonly Dictionary<long, HashSet<string>> _connections =
            new();

        public int Count => _connections.Count;

        public void Add(long key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    connections = new HashSet<string>();
                    _connections.Add(key, connections);
                }

                lock (connections)
                {
                    connections.Add(connectionId);
                }
            }
        }

        public IEnumerable<string> GetConnections(long key)
        {
            HashSet<string> connections;
            if (_connections.TryGetValue(key, out connections))
            {
                return connections;
            }

            return Enumerable.Empty<string>();
        }

        public void Remove(long key, string connectionId)
        {
            lock (_connections)
            {
                HashSet<string> connections;
                if (!_connections.TryGetValue(key, out connections))
                {
                    return;
                }

                lock (connections)
                {
                    connections.Remove(connectionId);

                    if (connections.Count == 0)
                    {
                        _connections.Remove(key);
                    }
                }
            }
        }
    }

    namespace Registrator
    {
        public class ConnectionsMappingServiceRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddSingleton<IConnectionsMappingService, ConnectionsMappingService>();
            }
        }
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Controllers
{
    /// <summary>
    /// Provide range validation of IList elements.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ElementRangeAttribute : ValidationAttribute
    {
        public ElementRangeAttribute(int minimum, int maximum)
        {
            this.Minimum = minimum;
            this.Maximum = maximum;
        }

        public ElementRangeAttribute(double minimum, double maximum)
        {
            this.Minimum = minimum;
            this.Maximum = maximum;
        }

        private double Minimum { get; set; }

        private double Maximum { get; set; }

        public override bool IsValid(object? value)
        {
            if (value is IList<int> intList)
            {
                return intList.All(element => element >= this.Minimum && element <= this.Maximum);
            }
            else if (value is IList<double> doubleList)
            {
                return doubleList.All(element => element >= this.Minimum && element <= this.Maximum);
            }
            else
                throw new InvalidOperationException("Attribute can be apply only on IList<int> or IList<double>.");
        }
    }
}

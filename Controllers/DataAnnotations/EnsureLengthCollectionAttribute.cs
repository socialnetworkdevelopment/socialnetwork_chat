﻿using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Controllers
{
    /// <summary>
    /// Provide length validation of ICollection.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class EnsureLengthCollectionAttribute : ValidationAttribute
    {
        public int MinLength { get; set; }
        
        public override bool IsValid(object? value)
        {
            var list = value as ICollection;
            if (list != null)
            {
                return list.Count >= this.MinLength;
            }
            return false;
        }
    }
}

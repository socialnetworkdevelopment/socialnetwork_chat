﻿using LanguageExt.Common;
using Microsoft.AspNetCore.Mvc;
using Utils;

namespace Controllers
{
    public static class ControllerExtentions
    {
        public static IActionResult ToOk<TResult, TContract, TObjectResult>(this Result<TResult> result,
            Func<TResult, TContract> mapper)
        {
            return result.Match(obj =>
            {
                TContract response = mapper(obj);
                return new OkObjectResult(response);
            }, 
            ProcessException);
        }

        public static IActionResult ToObjectResult<TResult>(this Maybe<TResult> result, int statusCode)
            where TResult : notnull
        {
            if (result.IsValue)
                return new ObjectResult(result.Value) { StatusCode = statusCode };
            else
                return ProcessException(result.Exception);
        }

        public static IActionResult ToOk<TResult>(this Maybe<TResult> result)
            where TResult : notnull
        {
            if (result.IsValue)
                return new OkObjectResult(result.Value);
            else
                return ProcessException(result.Exception);
        }

        public static IActionResult ToOk<TResult, TContract>(this Maybe<TResult> result, Func<TResult, TContract> mapper)
            where TResult : notnull
        {
            if (result.IsValue)
            {
                TContract response = mapper(result.Value);
                return new OkObjectResult(response);
            }
            else
                return ProcessException(result.Exception);
        }

        private static IActionResult ProcessException(Exception exception)
        {
            if (exception is ValueMissingException)
                return new NotFoundObjectResult(exception.Message);

            throw new Exception("Internal exception.", exception);
        }
    }
}

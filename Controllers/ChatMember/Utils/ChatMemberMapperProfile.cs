﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Controllers.ChatMember.Utils
{
    public class ChatMemberMapperProfile : Profile
    {
        public ChatMemberMapperProfile()
        {
            CreateMap<Dto.CreateChatMemberDto, UseCases.Dto.CreateChatMemberDto>();
            CreateMap<Dto.DeleteChatMemberDto, UseCases.Dto.DeleteChatMemberDto>();
        }
    }

    namespace Registration
    {
        public class ChatMemberMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new ChatMemberMapperProfile());
                });
            }
        }
    }
}

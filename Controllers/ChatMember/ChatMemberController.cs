﻿using ApplicationServices.Interfaces.EventPublisher;
using AutoMapper;
using Controllers.ChatMember.Dto;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.Create;

namespace Controllers.ChatMember
{
    [Authorize]
    [ApiController]
    [Route("api/chat_members")]
    public class ChatMemberController : ControllerBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;
        private readonly IEventPublisher _eventPublisher;

        public ChatMemberController(IMediator sender, IMapper mapper, IEventPublisher eventPublisher)
        {
            _sender = sender;
            _mapper = mapper;
            _eventPublisher = eventPublisher;
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([Required][FromBody] CreateChatMemberDto dto)
        {
            CreateChatMemberCommand command = new(_mapper.Map<UseCases.Dto.CreateChatMemberDto>(dto));
            var chatMemberInfo = await _sender.Send(command);
            
            await _eventPublisher.PublishAsync(command, PublishStrategy.ParallelNoWait);

            return new ObjectResult(chatMemberInfo.Id) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpDelete("")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete([FromQuery] int chatId, [FromQuery] long userId)
        {
            DeleteChatMemberCommand command = new(chatId, userId);
            await _sender.Send(command);

            await _eventPublisher.PublishAsync(command, PublishStrategy.ParallelNoWait);

            return NoContent();
        }
    }
}

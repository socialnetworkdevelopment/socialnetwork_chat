﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.ChatMember.Dto
{
    public class CreateChatMemberDto
    {
        [Range(1, int.MaxValue)]
        public int UserId { get; set; }

        [Range(1, int.MaxValue)]
        public int ChatId { get; set; }
    }
}

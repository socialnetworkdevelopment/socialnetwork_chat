﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.Chat.Dto
{
    public class UpdateChatDto
    {
        public string? Name { get; set; }

        public long? ImgId { get; set; }
    }
}

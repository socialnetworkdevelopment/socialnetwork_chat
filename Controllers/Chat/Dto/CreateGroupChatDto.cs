﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.Chat.Dto
{
    public class CreateGroupChatDto
    {
        [Required]
        public required string Name { get; set; }

        public string? ImgLink { get; set; }

        [Range(1, int.MaxValue)]
        public int CreatedBy { get; set; }

        [EnsureLengthCollection(MinLength = 1)]
        [ElementRange(1, int.MaxValue)]
        [Required]
        public required List<int> ChatMembers { get; set; }
    }
}

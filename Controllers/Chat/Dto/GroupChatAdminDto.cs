﻿namespace Controllers.Chat.Dto
{
    public class GroupChatAdminDto
    {
        public long Id { get; set; }

        public long UserId { get; set; }
    }
}

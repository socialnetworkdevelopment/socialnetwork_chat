﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.Chat.Dto
{
    public class CreateChatDto
    {
        [Range(1, int.MaxValue)]
        public int CreatedBy { get; set; }

        [Range(1, int.MaxValue)]
        public int СompanionId { get; set; }
    }
}

﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.AddAdminToChat;
using UseCases.Command.AddMemberToGroupChat;
using UseCases.Command.Create;
using UseCases.Command.CreateGroupChat;
using UseCases.Command.Delete;
using UseCases.Command.Update;
using UseCases.Dto;
using UseCases.Query.Get;
using UseCases.Query.GetByParticipantsId;
using UseCases.Query.GetByUserId;
using UseCases.Query.GetChatAdmins;
using UseCases.Query.GetUnread;
using Utils;

namespace Controllers.Chat
{
    [Authorize]
    [ApiController]
    [Route("api/chats")]
    public class ChatController : ControllerBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;

        public ChatController(IMediator sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        [HttpPost("")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Create([Required][FromBody] Dto.CreateChatDto dto)
        {
            CreateChatCommand command = new(_mapper.Map<CreateChatDto>(dto));
            int chatId = await _sender.Send(command);

            return new ObjectResult(chatId) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpPost("group")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateGroupChat([Required][FromBody] Dto.CreateGroupChatDto dto)
        {
            CreateGroupChatCommand command = new(_mapper.Map<CreateGroupChatDto>(dto));
            int chatId = await _sender.Send(command);

            return new ObjectResult(chatId) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpPut("group/{chatId}/addUser")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<ChatMemberInfo>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddMembersToGroupChat(int chatId, [FromBody] List<long> userIds)
        {
            AddMemberToGroupChatCommand command = new(chatId, userIds);
            var chatInfo = await _sender.Send(command);

            return new ObjectResult(_mapper.Map<List<Dto.ChatMemberInfo>>(chatInfo)) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ChatDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Get([Range(1, int.MaxValue)] int id)
        {
            GetChatQuery command = new(id);
            Maybe<ChatDto> result = await _sender.Send(command);

            return result.ToOk(_mapper.Map<Dto.ChatDto>);
        }

        [HttpGet("")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IList<ChatDto>))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByUserId([Range(1, long.MaxValue)] long userId)
        {
            GetChatByUserIdQuery command = new(userId);
            IList<ChatDto> result = await _sender.Send(command);

            return Ok(result);
        }

        [HttpGet("getByParticipants")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ChatDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetPrivateByParticipantsId([Range(1, long.MaxValue)] long participantId1, [Range(1, long.MaxValue)] long participantId2)
        {
            GetChatByParticipantsIdQuery query = new(participantId1, participantId2);
            Maybe<ChatDto> result = await _sender.Send(query);

            return result.ToOk(_mapper.Map<Dto.ChatDto>);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Delete([Range(1, int.MaxValue)] int id)
        {
            DeleteChatCommand command = new(id);
            await _sender.Send(command);

            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> Update(int id, [Required][FromBody] Dto.UpdateChatDto chatDto)
        {
            var dto = _mapper.Map<UpdateChatDto>(chatDto);
            dto.Id = id;
            UpdateChatCommand command = new(dto);
            Maybe<int> chatId = await _sender.Send(command);
            return chatId.ToOk();
        }

        [HttpGet("{id}/unreadMessagesCount")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetUnreadMessageCount([Range(1, int.MaxValue)] int id, [FromQuery][Range(1, long.MaxValue)] int userId)
        {
            GetUnreadMessagesCountQuery command = new(userId, id);
            int result = await _sender.Send(command);

            return Ok(result);
        }

        [HttpPost("{id}/addGroupAdmin")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddGroupChatAdmin([Range(1, int.MaxValue)] int id, [FromBody][Range(1, long.MaxValue)] long userId)
        {
            AddAdminToChatCommand command = new(id, userId);
            long result = await _sender.Send(command);

            return Ok(result);
        }

        [HttpGet("{id}/groupAdmins")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IList<Dto.GroupChatAdminDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetGroupAdmins([Range(1, int.MaxValue)] int id)
        {
            GetChatAdminsQuery query = new(id);
            IList<ChatAdminDto> result = await _sender.Send(query);
            var chatAdmins = _mapper.Map<List<Dto.GroupChatAdminDto>>(result);

            return Ok(chatAdmins);
        }
    }
}

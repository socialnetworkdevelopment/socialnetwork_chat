﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Controllers.Chat.Utils
{
    public class ChatMapperProfile : Profile
    {
        public ChatMapperProfile()
        {
            CreateMap<Dto.UpdateChatDto, UseCases.Dto.UpdateChatDto>()
                .ForMember(dest => dest.Id, opt => opt.Ignore());
            CreateMap<Dto.CreateChatDto, UseCases.Dto.CreateChatDto>();
            CreateMap<Dto.CreateGroupChatDto, UseCases.Dto.CreateGroupChatDto>();
            CreateMap<Dto.ChatDto, UseCases.Dto.ChatDto>()
                .ReverseMap();
            CreateMap<UseCases.Dto.ChatMemberInfo, Dto.ChatMemberInfo>();
            CreateMap<UseCases.Dto.ChatAdminDto, Dto.GroupChatAdminDto>();
        }
    }

    namespace Registration
    {
        public class ChatMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new ChatMapperProfile());
                });
            }
        }
    }
}

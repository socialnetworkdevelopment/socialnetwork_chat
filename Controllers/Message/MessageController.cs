﻿using ApplicationServices.Interfaces.EventPublisher;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using UseCases.Command.Create;
using UseCases.Command.Read;
using UseCases.Dto;
using UseCases.Handlers.Dto;
using UseCases.Handlers.Notifications.ReadMessage;
using UseCases.Handlers.Notifications.SendMessage;
using UseCases.Query.Get;
using UseCases.Query.GetSelectively;

namespace Controllers.Message
{
    [Authorize]
    [ApiController]
    [Route("api/messages")]
    public class MessageController : ControllerBase
    {
        private readonly IMediator _sender;
        private readonly IMapper _mapper;
        private readonly IEventPublisher _eventPublisher;

        public MessageController(IMediator sender, IMapper mapper, IEventPublisher eventPublisher)
        {
            _sender = sender;
            _mapper = mapper;
            _eventPublisher = eventPublisher;
        }

        [HttpGet()]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Dto.MessageDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByChat([Required][FromQuery] int chatId, [FromQuery] int? pageNumber, [FromQuery] int? pageSize)
        {
            GetMessagesQuery query = new(chatId, pageNumber, pageSize);
            var messages = await _sender.Send(query);
            var messagesDto = _mapper.Map<List<Dto.MessageDto>>(messages);

            return Ok(messagesDto);
        }

        [HttpGet("search")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(List<Dto.MessageDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByChatSelectively([Required][FromQuery] Dto.GetSelectivelyMessagesDto dto)
        {
            GetSelectivelyMessagesQuery query = new(_mapper.Map<GetSelectivelyMessagesDto>(dto));
            var messages = await _sender.Send(query);
            var messagesDto = _mapper.Map<List<Dto.MessageDto>>(messages);

            return Ok(messagesDto);
        }

        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(long))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> CreateMessage([Required][FromBody] Dto.CreateMessageDto dto)
        {
            CreateMessageCommand command = new(_mapper.Map<CreateMessageDto>(dto));
            var message = await _sender.Send(command);

            SendMessageNotification notification = new(_mapper.Map<SendMessageDto>(message));
            await _eventPublisher.PublishAsync(notification, PublishStrategy.ParallelNoWait);

            return new ObjectResult(message.Id) { StatusCode = StatusCodes.Status201Created };
        }

        [HttpPut("read")]
        [ProducesResponseType(StatusCodes.Status204NoContent, Type = typeof(int))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> ReadMessage([Required][FromBody] Dto.ReadMessageDto dto)
        {
            ReadMessageCommand command = new(_mapper.Map<ReadMessageDto>(dto));
            _ = await _sender.Send(command);

            ReadMessageNotification notification = new(_mapper.Map<ReadMessageDto>(dto));
            await _eventPublisher.PublishAsync(notification, PublishStrategy.SyncStopOnException);

            return NoContent();
        }
    }
}

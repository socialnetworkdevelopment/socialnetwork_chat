﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.Message.Dto
{
    public class CreateMessageDto
    {
        [StringLength(maximumLength: 4096, MinimumLength = 1)]
        public required string Text { get; set; }

        [Range(1, int.MaxValue)]
        public int AuthorId { get; set; }

        [Range(1, int.MaxValue)]
        public int ChatId { get; set; }
    }
}

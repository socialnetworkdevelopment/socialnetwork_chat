﻿namespace Controllers.Message.Dto
{
    public class MessageDto
    {
        public int Id { get; set; }

        public required string Text { get; set; }

        public int AuthorId { get; set; }

        public DateTime SendAt { get; set; }

        public bool IsRead { get; set; }
    }
}

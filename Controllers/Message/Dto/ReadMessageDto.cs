﻿using System.ComponentModel.DataAnnotations;

namespace Controllers.Message.Dto
{
    public class ReadMessageDto
    {
        [Range(1, int.MaxValue)]
        public int MessageId { get; set; }

        [Range(1, int.MaxValue)]
        public int ChatId { get; set; }
    }
}

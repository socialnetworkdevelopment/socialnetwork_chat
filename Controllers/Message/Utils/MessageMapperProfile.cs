﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Utils;

namespace Controllers.Message.Utils
{
    internal class MessageMapperProfile : Profile
    {
        public MessageMapperProfile() 
        {
            CreateMap<Dto.CreateMessageDto, UseCases.Dto.CreateMessageDto>();
            CreateMap<Dto.ReadMessageDto, UseCases.Dto.ReadMessageDto>();
            CreateMap<Dto.GetSelectivelyMessagesDto, UseCases.Dto.GetSelectivelyMessagesDto>();
            CreateMap<UseCases.Dto.MessageDto, Dto.MessageDto>();
            CreateMap<Entities.Message, UseCases.Handlers.Dto.SendMessageDto>()
                .ForMember(dest => dest.AuthorId, opt => opt.MapFrom(src => src.Author.Id))
                .ForMember(dest => dest.ChatId, opt => opt.MapFrom(src => src.Chat.Id))
                .ForMember(dest => dest.SendAt, opt => opt.MapFrom(src => src.CreatedAt));
        }
    }

    namespace Registration
    {
        public class MessageMapperProfileRegistrator : IServiceRegistrator
        {
            public void AddServices(IServiceCollection services)
            {
                Check.NotNull(services).AddAutoMapper((mapperConfiguration) =>
                {
                    mapperConfiguration.AddProfile(new MessageMapperProfile());
                });
            }
        }
    }
}
